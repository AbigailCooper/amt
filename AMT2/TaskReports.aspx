﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TaskReports.aspx.cs" Inherits="AMT2.TaskReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="EPA.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Task Reports</h1>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Task Reports are very similar to the logs you wrote for your Level 4. Here, we will help you keep track of them. "></asp:Label>
    <br />
    <asp:Label runat="server" CssClass="label" ID="label"></asp:Label>
    <asp:Table runat="server">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>1</asp:TableHeaderCell>
            <asp:TableHeaderCell>2</asp:TableHeaderCell>
            <asp:TableHeaderCell>3</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell><asp:CheckBox runat="server" ID="checkbox1" /></asp:TableCell>
            <asp:TableCell><asp:CheckBox runat="server" ID="checkbox2" /></asp:TableCell>
            <asp:TableCell><asp:CheckBox runat="server" ID="checkbox3" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />

    <asp:Button runat="server" id="save" CssClass="submit" Text="Save" OnClick="save_Click" />
    <br />
    <asp:Label runat="server" ID="error" CssClass="error" style="top:400px; left:400px"></asp:Label>
    <br />
</asp:Content>
