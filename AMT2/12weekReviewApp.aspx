﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="12weekReviewApp.aspx.cs" Inherits="AMT2._12weekReviewApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />

    <h1 id="heading1">12 Week Review</h1>

    <%-- Display the apprentice review in a gridview --%>
        <asp:Panel runat="server" CssClass="panelReview">
            <asp:GridView runat="server" ID="apprenticeReview" class="apprenticeReview" ShowHeader="true" GridLines="Horizontal" AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Reviews Yet.">
                <Columns>
                    <asp:BoundField DataField="dateOfReview" HeaderText="Date of Review" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="healthAndSafety" HeaderText="Health and Safety" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="knowledgeAndUnderstanding" HeaderText="Knowlegde and Understanding" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="designProcesses"  HeaderText="Design Processes" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="responsibilityManagement" HeaderText="Responsibility and Management" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="commsSkills" HeaderText="Communication Skills" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="profCommitment"  HeaderText="Professional Commitment" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="equalityDiversity" HeaderText="Equality and Diversity" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="attendance" HeaderText="Attendance" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="generalCommentsManager" HeaderText="Comments from Manager" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="generalCommentsAssessor" HeaderText="Comments from Assessor" HeaderStyle-Font-Bold="true" />
                </Columns>
                
            </asp:GridView>

        </asp:Panel>
</asp:Content>
