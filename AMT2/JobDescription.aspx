﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="JobDescription.aspx.cs" Inherits="AMT2.JobDescription" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="EPA.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Job Description</h1>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Write a job description for your current role. "></asp:Label>
    <br />
    <asp:TextBox runat="server" CssClass="jobDesc" ID="jobDesc" ></asp:TextBox>
    <br />
    <asp:Button runat="server" ID="save" CssClass="submit" Text="Save" OnClick="save_Click"/>
    <br />
    <br />
    <asp:Label runat="server" ID="error" CssClass="error" Visible="false" style="top:486px; left:34px"  ></asp:Label>
</asp:Content>
