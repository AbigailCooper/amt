﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="reviewObjectives.aspx.cs" Inherits="AMT2.reviewObjectives" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    

    <br />
    <br />

    <h2>Review Objectives</h2>
    <br />
        <asp:Label runat="server" CssClass="objectiveTitle2" ID="objectiveTitle2" Text="Your current Objectives: "></asp:Label>
        <br />
        <asp:TextBox runat="server" class="objectivesReview" ID="objectives" TextMode="MultiLine" Columns="50"></asp:TextBox>
        <br />
        <br />
        <asp:TextBox runat="server" ID="Update" class="updates" Placeholder="Write review here..." BackColor="LightSlateGray" ></asp:TextBox>
        <br />
        <asp:Button runat="server" ID="SaveUpdate" CssClass="submitupdates" OnClick="SaveUpdate_Click" Text="Save Update" />
        <br /><br />
        <%-- Display the updates in a gridview --%>
        <asp:Panel runat="server" CssClass="panelUpdate">
            <asp:GridView runat="server" ID="objUpdates" class="objUpdates" ShowHeader="false" GridLines="Horizontal" AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Updates Yet.">
                <Columns>
                    <asp:BoundField DataField="username" />
                    <asp:BoundField DataField="note" />
                    <asp:BoundField DataField="entryDate" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
</asp:Content>
