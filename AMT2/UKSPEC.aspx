﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UKSPEC.aspx.cs" Inherits="AMT2.UKSPEC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <asp:ImageButton runat="server" ImageUrl="~/img/back.png" ID="back" AlternateText="Back" CssClass="backArrow1" OnClick="Unnamed_Click" />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />

    <h1 id="heading1">The Incorporated Engineer Standard</h1>
    <br />
    <p>Throughout the apprenticeship, you must meet the various elements of the UKSpec.</p>
    
    <asp:Table CssClass="table1" ID="Table1" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button CssClass="ukspecButton" ID="btnShow" runat="server" Text="Add Mapping to UKSPEC" OnClick="btnShow_Click" /><%-- This button displays form to add UKPSEC mapping. --%>
                <%-- ---------------------------------------------- --%>
                <%-- Pop-up form for adding a mapping to the UKSPEC --%>
                <%-- ---------------------------------------------- --%>
                <br />
                <asp:Label ID="labelSaved" runat="server" class="label" ></asp:Label>
                <asp:Panel ID="FormAddUKSPEC" runat="server" ScrollBars="Auto" visible="false">
                    <asp:Label Class="label" ID="DateOfEnrichmentLbl" runat="server" Text="Date of Enrichment"></asp:Label>
                    <asp:Calendar  ID="EnrichmentCalendar" runat="server">
                        <DayHeaderStyle  BackColor="#cccccc"/>
                        <SelectedDayStyle BackColor="OrangeRed" ForeColor="White" />
                    </asp:Calendar>

                    <br />
                    <asp:Label Class="label" ID="LocationLbl" runat="server" Text="Location of Enrichment"></asp:Label>
                    <asp:TextBox Class="text" ID="Location" runat="server"></asp:TextBox>

                    <br />
                    <asp:Label Class="label" ID="CourseLbl" runat="server" Text="Course Name or Event"></asp:Label>
                    <asp:TextBox Class="text" ID="CourseName" runat="server"></asp:TextBox>

                    <br />
                    <asp:Label Class="label" ID="EventLeadLbl" runat="server" Text="Event Lead"></asp:Label>
                    <asp:TextBox Class="text" ID="EventLead" runat="server"></asp:TextBox>

                    <br />
                    <asp:Label class="label" ID="TimeSpentLbl" runat="server" Text="Hours Spent "></asp:Label>
                    <asp:TextBox Class="text" ID="TimeSpent" runat="server"></asp:TextBox>
                    <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Double" 
                        ControlToValidate="TimeSpent" ErrorMessage="Value must be a number, in hours." CssClass="error" />

                    <br />
                    <asp:Label Class="label" ID="BehavioursLbl" runat="server" Text="Behaviours Developed: "></asp:Label>
                    <asp:CheckBoxList ID="Behaviours" runat="server">
                        <asp:ListItem Text="A1"></asp:ListItem>
                        <asp:ListItem Text="A2"></asp:ListItem>
                        <asp:ListItem Text="B1"></asp:ListItem>
                        <asp:ListItem Text="B2"></asp:ListItem>
                        <asp:ListItem Text="B3"></asp:ListItem>
                        <asp:ListItem Text="C1"></asp:ListItem>
                        <asp:ListItem Text="C2"></asp:ListItem>
                        <asp:ListItem Text="C3"></asp:ListItem>
                        <asp:ListItem Text="C4"></asp:ListItem>
                        <asp:ListItem Text="D1"></asp:ListItem>
                        <asp:ListItem Text="D2"></asp:ListItem>
                        <asp:ListItem Text="D3"></asp:ListItem>
                        <asp:ListItem Text="E1"></asp:ListItem>
                        <asp:ListItem Text="E2"></asp:ListItem>
                        <asp:ListItem Text="E3"></asp:ListItem>
                        <asp:ListItem Text="E4"></asp:ListItem>
                        <asp:ListItem Text="E5"></asp:ListItem>
                    </asp:CheckBoxList>

                    <br />

                    <asp:Button class="submit" ID="Save_UKSPEC" runat="server" Text="Save" onClick="Save_UKSPEC_Click" />

                </asp:Panel>
                <%-- ------------------------------------ --%>
                <%-- Display apprentices current mappings --%>
                <%-- ------------------------------------ --%>
                <br />
                <asp:Panel runat="server">
                    <asp:GridView runat="server" ID="UKSPECdata"  AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Data Available." >
                        <HeaderStyle BackColor="#D0D3D4" ForeColor="Black" Font-Bold="true" />
                        <Columns>
                            <asp:BoundField DataField="date" HeaderText="Date" />
                            <asp:BoundField DataField="location" HeaderText="Location" />
                            <asp:BoundField DataField="eventName" HeaderText="Event Name" />
                            <asp:BoundField DataField="eventLead" HeaderText="Event Lead" />
                            <asp:BoundField DataField="timeSpent" HeaderText="Hours Spent" />
                            <asp:BoundField DataField="behavioursDeveloped" HeaderText="Behaviours Developed" />
                        </Columns>
                        
                    </asp:GridView>

                </asp:Panel>
                <br />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Incorporated Engineers are able to demonstrate:
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell style="color: orangered">
                <br />
                <b>A.</b> The theoretical knowledge to solve problems in developed technologies using analytical techniques. 
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        A1.</b> Maintain and extend a sound theoretical approach to the application of technology in practice.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        A2.</b> Use a sound evidence-based approach to problem solving and contribute to CI.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell style="color: orangered">
                <br />
                <b>B.</b>  Apply appropriate theoretical and practical methods to design, develop, maintain and decommision engineering processes and systems.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        B1.</b> Identify, review and select techniques, procedures and methods to undertake engineering tasks.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        B2.</b> Contribute to the design and development of engineering solutions.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        B3.</b> Implement design solutions and contribute to their evaluation.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell style="color: orangered">
                <br />
                <b>C.</b> Provide technical and commercial management.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        C1.</b> Plan for effective project implementation.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        C2.</b> Manage tasks, people and resources to plan and budget.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        C3.</b> Manage teams and develop staff to meet changing technical and managerial needs.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        C4.</b> Manage continuous quality improvement.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell style="color: orangered">
                <br />
                <b>D.</b> Demonstrate effective interpersonal skills.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        D1.</b> Communicate in English with others at all levels.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        D2.</b> Present and discuss proposals.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        D3.</b> Demonstrate personal and social skills.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell style="color: orangered">
                <br />
                <b>E.</b> Commitment to professional engineering values, society, the profession and the environment.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        E1.</b> Comply with relevant codes of conduct.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        E2.</b> Manage and apply safe systems of work.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        E3.</b> Undertake engineering activities in a way that contributes to sustainable development.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        E4.</b> Carry out and record CPD necessary to maintain and enchance competence in own area.
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <b>        E5.</b> Exercise responsibilities in an ethical manner.
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
