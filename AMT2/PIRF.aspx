﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PIRF.aspx.cs" Inherits="AMT2.PIRF" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="EPA.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Professional Indicators Recording Form</h1>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="The PIRF is a record of evidence to show that you are professionally competent. It is alinged to the IEng standards, so on this page there is a table of the things you have mapped against the UKSPEC as this is also aligned to the IEng. This should aid you in writing your PIRF document. "></asp:Label>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="UKSPEC Mappings: "></asp:Label><br />
    <asp:Panel runat="server">
        <asp:GridView runat="server" ID="UKSPECdata" AutoGenerateColumns="false" EmptyDataText="No Data Available.">
            <HeaderStyle BackColor="#D0D3D4" ForeColor="Black" Font-Bold="true" />
            <Columns>
                <asp:BoundField DataField="date" HeaderText="Date" />
                <asp:BoundField DataField="location" HeaderText="Location" />
                <asp:BoundField DataField="eventName" HeaderText="Event Name" />
                <asp:BoundField DataField="eventLead" HeaderText="Event Lead" />
                <asp:BoundField DataField="timeSpent" HeaderText="Time Spent" />
                <asp:BoundField DataField="behavioursDeveloped" HeaderText="Behaviours Developed" />
            </Columns>

        </asp:GridView>

    </asp:Panel>

    <br />
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Objectives from your placements: "></asp:Label><br />
    <asp:Panel runat="server">
        <asp:Table runat="server" ID="table">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell>
                    <asp:Label runat="server" CssClass="label" Text="Placement"></asp:Label>
                </asp:TableHeaderCell>
                <asp:TableHeaderCell>
                    <asp:Label runat="server" CssClass="label" Text="Objectives"></asp:Label>
                </asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Placement1"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Objective1"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Placement2"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Objective2"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Placement3"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Objective3"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Placement4"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Objective4"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Placement5"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label" ID="Objective5"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <br />
    <asp:Label runat="server" ID="error" CssClass="error" Visible="false"></asp:Label>

</asp:Content>
