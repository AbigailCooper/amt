﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class PlacementInductionManager : System.Web.UI.Page
    {
        string usernameS;
        string managerID;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            

            //---------------------------
            //Fill the Apprentice Dropdown
            //---------------------------
            SqlCommand query = new SqlCommand("getUserID", con);
            query.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            query.Parameters.Add(p1);
            con.Open();
            managerID = query.ExecuteScalar().ToString();
            con.Close();

            SqlCommand query2 = new SqlCommand("SELECT apprenticeID FROM apprenticePlacement WHERE managerID=@managerid", con);
            query2.Parameters.AddWithValue("@managerid", managerID);
            con.Open();

            List<string> apprenticesId = new List<string>();


            using (SqlDataReader reader = query2.ExecuteReader())
            {
                while (reader.Read())
                {
                    apprenticesId.Add(reader.GetString(0));
                }
            }
            con.Close();

            if (!this.IsPostBack)
            {
                DataSet ds = new DataSet();
                for (int i = 0; i < apprenticesId.Count; i++)
                {
                    SqlCommand com = new SqlCommand("SELECT username FROM users WHERE uID=@userID");
                    com.Parameters.AddWithValue("@userID", apprenticesId[i]);
                    using (com)
                    {
                        com.CommandType = CommandType.Text;
                        com.Connection = con;
                        using (SqlDataAdapter da = new SqlDataAdapter(com))
                        {

                            da.Fill(ds);
                            apprentices.DataSource = ds.Tables[0];
                            apprentices.DataTextField = "username";
                            apprentices.DataValueField = "username";

                            apprentices.DataBind();
                        }
                    }
                }


                try
                {
                    if (apprentices.Items.FindByValue("-- Select Apprentice --").Value == null) { }
                }
                catch (NullReferenceException)
                {
                    apprentices.Items.Insert(0, new ListItem("--Select Apprentice--", "0"));
                }


            }
        }

        protected void apprentices_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset the values -> this means it will be clear if the user selects index 0.
            obj1.Text = "";
            obj2.Text = "";
            obj3.Text = "";
            obj4.Text = "";
            obj5.Text = "";
            updateTable(); // update the notes table.
            string objectives = "";
            // Get objectives from database for selected apprentice
            if(apprentices.SelectedIndex == 0)
            {
                error.Visible = true;
                error.Text = "Please select a apprentice.";
            }
            else
            {
                string apprenticeUsername = apprentices.SelectedValue;
                string appPlacementID = getAppPlacementID();
                SqlCommand com = new SqlCommand("SELECT objectives FROM placementInduction WHERE appPlacementID=@appPlace", con);
                com.Parameters.AddWithValue("@appPlace", appPlacementID);
                con.Open();
                objectives = com.ExecuteScalar().ToString();
                con.Close();
                if (objectives == null)
                {
                    error.Visible = true;
                    error.Text = "This apprentice has no objectives set. Please sit down with them to set some.";
                }
                else
                {

                    char[] separator = { '$' };
                    Int32 count = 5;

                    String[] strlist = objectives.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < strlist.Length; i++)
                    {
                        String num = (i + 1).ToString();
                        if (i == 0)
                        {
                            obj1.Text += num + ". ";
                            obj1.Text += strlist[i].ToString();
                        }
                        else if (i == 1)
                        {
                            obj2.Text += num + ". ";
                            obj2.Text += strlist[i].ToString();
                        }
                        else if (i == 2)
                        {
                            obj3.Text += num + ". ";
                            obj3.Text += strlist[i].ToString();
                        }
                        else if (i == 3)
                        {
                            obj4.Text += num + ". ";
                            obj4.Text += strlist[i].ToString();
                        }
                        else if (i == 4)
                        {
                            obj5.Text += num + ". ";
                            obj5.Text += strlist[i].ToString();
                        }

                    }
                }

            }
        }

        private string getAppPlacementID()
        {
            //First find the selected apprentices ID
            SqlCommand query = new SqlCommand("getUserID", con);
            query.CommandType = System.Data.CommandType.StoredProcedure;

            if (apprentices.SelectedValue == "0")
            {
                return ""; // No apprentice is selected.
            }
            else
            {
                SqlParameter p1 = new SqlParameter("username", apprentices.SelectedValue.ToString());
                query.Parameters.Add(p1);
                con.Open();
                string appID = query.ExecuteScalar().ToString();
                con.Close();

                SqlCommand com2 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@appID AND managerID=@managerID", con);
                com2.Parameters.AddWithValue("@appID", appID);
                com2.Parameters.AddWithValue("@managerID", managerID);
                con.Open();
                string appPlacementID = com2.ExecuteScalar().ToString();
                con.Close();
                return appPlacementID;
            }
        }

        protected void SaveUpdate_Click(object sender, EventArgs e)
        {
            string ID = UID();
            DateTime date = DateTime.Now;
            string placementInductionID = getPlacementInductionID();
            SqlCommand cmd = new SqlCommand("INSERT INTO notes VALUES(@noteID, @note, @entryDate, @username, @placementInductionID)", con);
            cmd.Parameters.AddWithValue("@noteID", ID);
            cmd.Parameters.AddWithValue("@note", Update.Text);
            cmd.Parameters.AddWithValue("@entryDate", date);
            cmd.Parameters.AddWithValue("@username", usernameS);
            cmd.Parameters.AddWithValue("@placementInductionID", placementInductionID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Update.Text = ""; // Clear the text box.
            Response.Redirect("PlacementInductionManager.aspx"); //Refresh the page.
        }

        // Create a unique ID.
        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }        

        private string getPlacementInductionID()
        {
            string appPlacementID = getAppPlacementID();
            SqlCommand cmd = new SqlCommand("SELECT id FROM placementInduction WHERE appPlacementID=@appPlacementID", con);
            cmd.Parameters.AddWithValue("@appPlacementID", appPlacementID);
            con.Open();
            string placementInductionID = cmd.ExecuteScalar().ToString();
            con.Close();

            return placementInductionID;
        }

        private void updateTable()
        {
            //Get data from database
            if (apprentices.SelectedValue == "0")
            {
                objUpdates.Visible = false;
            }
            else
            {
                objUpdates.Visible = true;
                string placementInductionID = getPlacementInductionID();

                String query = "SELECT note, username, entryDate FROM Notes WHERE placementInductionID='" + placementInductionID + "' ORDER BY entryDate DESC";

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    objUpdates.DataSource = dt;
                    objUpdates.DataBind();
                }
            }
        }
    }
}