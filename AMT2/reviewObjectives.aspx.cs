﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class reviewObjectives : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            updateTable();

            //--------------------
            // Fill in Objectives
            //--------------------

            //First get uID to lookup in placement table.
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            //Find endDate of placement
            SqlCommand cmd3 = new SqlCommand("SELECT endDate FROM apprenticePlacement WHERE startDate =( SELECT MAX(startDate) FROM apprenticePlacement WHERE apprenticeID=@apprenticeID)", con);
            cmd3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            DateTime endDate = new DateTime();
            endDate = (DateTime)cmd3.ExecuteScalar();
            con.Close();
            

            SqlCommand cmd4 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@uID AND endDate=@endDate", con);
            cmd4.Parameters.AddWithValue("@uID", apprenticeID);
            cmd4.Parameters.AddWithValue("@endDate", endDate);
            con.Open();
            string appPlacement = null;
            try
            {
                appPlacement = cmd4.ExecuteScalar().ToString();
            }
            catch (NullReferenceException)
            {
                //ERROR
            }
            con.Close();
            SqlCommand cmd5 = new SqlCommand("SELECT objectives FROM placementInduction WHERE appPlacementID=@appPlacement", con);
            cmd5.Parameters.AddWithValue("@appPlacement", appPlacement);
            con.Open();
            string objs = cmd5.ExecuteScalar().ToString();

            char[] separator = { '$' };
            Int32 count = 5;

            String[] strlist = objs.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < strlist.Length; i++)
            {
                String num = (i + 1).ToString();
                objectives.Text += num + ". ";
                objectives.Text += strlist[i].ToString() + "\r\n";
            }
            con.Close();
        }

        protected void SaveUpdate_Click(object sender, EventArgs e)
        {
            string ID = UID();
            DateTime date = DateTime.Now;
            string placementInductionID = getPlacementInductionID(usernameS);
            SqlCommand cmd = new SqlCommand("INSERT INTO notes VALUES(@noteID, @note, @entryDate, @username, @placementInductionID)", con);
            cmd.Parameters.AddWithValue("@noteID", ID);
            cmd.Parameters.AddWithValue("@note", Update.Text);
            cmd.Parameters.AddWithValue("@entryDate", date);
            cmd.Parameters.AddWithValue("@username", usernameS);
            cmd.Parameters.AddWithValue("@placementInductionID", placementInductionID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Update.Text = ""; // Clear the text box.
            Response.Redirect("reviewObjectives.aspx"); //Refresh the page.
        }

        public string getPlacementInductionID(string username)
        {
            string appPlacementID = getApprenticePlaceID(username);
            SqlCommand cmd = new SqlCommand("SELECT id FROM placementInduction WHERE appPlacementID=@appPlacementID", con);
            cmd.Parameters.AddWithValue("@appPlacementID", appPlacementID);
            con.Open();
            string placementInductionID = cmd.ExecuteScalar().ToString();
            con.Close();

            return placementInductionID;
        }

        // Create a unique ID.
        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        // Retrieve the apprentice placement ID from the appPlacement table using the session name.
        private string getApprenticePlaceID(string username)
        {
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", username);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            SqlCommand cmd3 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@apprenticeID", con);
            cmd3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            string appPlacementID = cmd3.ExecuteScalar().ToString();
            con.Close();

            return appPlacementID;
        }

        private void updateTable()
        {
            //Get data from database
            string placementInductionID = getPlacementInductionID(usernameS);

            String query = "SELECT note, username, entryDate FROM Notes WHERE placementInductionID='" + placementInductionID + "' ORDER BY entryDate DESC";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                objUpdates.DataSource = dt;
                objUpdates.DataBind();
                con.Close();
            }
        }
    }
}