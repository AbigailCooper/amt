﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="12weekReviewEC.aspx.cs" Inherits="AMT2._12weekReviewEC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />

    <h1 id="heading1">12 Week Review</h1>

    <asp:Label runat="server">Select an apprentice</asp:Label>
    <asp:DropDownList runat="server" CssClass="dropdownapp" OnSelectedIndexChanged="apprentices_SelectedIndexChanged" ValidationGroup="g1" AutoPostBack="true" ID="apprentices"></asp:DropDownList>
    <asp:Label runat="server" ID="errorMessage" CssClass="error" Visible="false"></asp:Label>
    <asp:CompareValidator ControlToValidate="apprentices" ID="CompareValidator1" ValidationGroup="g1" CssClass="errormesg" ErrorMessage="Please select an apprentice."
        runat="server" Display="Dynamic" Operator="NotEqual" ValueToCompare="0" Type="Integer" style="top: 189px; left: 335px" />
    
    <br />
    <br />
    <br />
     <%-- Display the apprentice review in a gridview --%>
        <asp:Panel runat="server" CssClass="panelReview">
            <asp:GridView runat="server" ID="apprenticeReview" class="apprenticeReview" ShowHeader="true" AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Reviews Yet.">
                <Columns>
                    <asp:BoundField DataField="dateOfReview" HeaderText="Date of Review" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="healthAndSafety" HeaderText="Health and Safety" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="knowledgeAndUnderstanding" HeaderText="Knowlegde and Understanding" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="designProcesses"  HeaderText="Design Processes" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="responsibilityManagement" HeaderText="Responsibility and Management" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="commsSkills" HeaderText="Communication Skills" HeaderStyle-Font-Bold="true" />
                    <asp:BoundField DataField="profCommitment"  HeaderText="Professional Commitment" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="equalityDiversity" HeaderText="Equality and Diversity" HeaderStyle-Font-Bold="true"/>
                    <asp:BoundField DataField="attendance" HeaderText="Attendance" HeaderStyle-Font-Bold="true" />
                </Columns>
                
            </asp:GridView>

        </asp:Panel>
    <br />
    <br />
    <br />
    <asp:Label runat="server" Text="Add comments: "></asp:Label>
    <asp:TextBox runat="server" id="generalComsEC" type="text" ></asp:TextBox>
    <br />
    <asp:Button runat="server" text="Save" CssClass="submit" ID="saveComments" OnClick="SaveComments_Click" ValidationGroup="g1" /> <br />
    <asp:Label runat="server" Text="Error submitting to the database." ID="error" CssClass="error" Visible="false"></asp:Label>

</asp:Content>
