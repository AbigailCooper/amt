﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class Home : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }
            error.Visible = false;

            if(isUserManager())
            {
                UKSPECTile.Visible = false;
                Level4Tile.Visible = false;
            }
            
        }

        protected void UKSPECTile_Click(object sender, ImageClickEventArgs e)
        {
            if(isUserApprentice())
            {
                Response.Redirect("UKSPEC.aspx");
            }
            else if(isUserManager())
            {
                error.Visible = true;
                error.Text = "As a manager, you do not have access to the page.";
            }
        }

        protected bool isUserManager()
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "manager")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool isUserEC()
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "earlyCareers")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool isUserApprentice()
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "apprentice")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void EPATile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("EPA.aspx");
        }

        protected void Level4Tile_Click(object sender, ImageClickEventArgs e)
        {
            if (isUserApprentice())
            {
                Response.Redirect("Level4.aspx");
            }
            else if(isUserEC())
            {
                Response.Redirect("https://system.learningassistant.com/bae/"); //Take EC to learning assistant - they will get more info from that tool.
            }
            else if(isUserManager())
            {
                error.Visible = true;
                error.Text = "As a manager, you do not have access to the page.";
            }
        }

        protected void WorkPlacementTile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("WorkPlacement.aspx");
        }
    }
}