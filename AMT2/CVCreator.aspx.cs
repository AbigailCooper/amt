﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Word;

namespace AMT2
{
    public partial class CVCreator : System.Web.UI.Page
    {
        string usernameS;
        string apprenticeID;
        int flag = 0;
        string placement1Obj = "";
        string placement2Obj = "";
        string placement3Obj = "";
        string placement4Obj = "";
        string placement5Obj = "";
        string scheme;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");


        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //---------------------
            //Degree Year drop down
            //---------------------
            ListItem newItem = new ListItem();
            newItem.Text = "-- Select --"; //Default
            newItem.Value = "0";
            degreeYear.Items.Add(newItem);
            level4Year.Items.Add(newItem);

            DateTime now = DateTime.Now;
            int yearNow = now.Year;

            ListItem year1 = new ListItem();
            year1.Text = yearNow.ToString();
            year1.Value = "1";
            degreeYear.Items.Add(year1);
            level4Year.Items.Add(year1);

            int lastYear = yearNow - 1;
            ListItem year2 = new ListItem();
            year2.Text = lastYear.ToString();
            year2.Value = "2";
            degreeYear.Items.Add(year2);
            level4Year.Items.Add(year2);

            //----------
            //Get userID
            //----------
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            //------------------------------
            //Get users scheme from database
            //------------------------------
            SqlCommand com3 = new SqlCommand("SELECT scheme FROM apprentices WHERE uID=@uID", con);
            com3.Parameters.AddWithValue("@uID", apprenticeID);
            con.Open();
            scheme = com3.ExecuteScalar().ToString();
            con.Close();
            if(scheme == "Software")
            {
                languages.Visible = true;
                languagesLabel.Visible = true;
            }
        }

        protected void save_Click(object sender, EventArgs e)
        {
            //OBJECT OF MISSING "NULL VALUE"

            Object oMissing = System.Reflection.Missing.Value;

            Object oTemplatePath = "C:\\Users\\abiga\\Documents\\Custom Office Templates\\CVTemplate1.dotx";


            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Document wordDoc = new Document();

            wordDoc = wordApp.Documents.Add(ref oTemplatePath, ref oMissing, ref oMissing, ref oMissing);

            //----------------------------
            //Get users name from database
            //----------------------------
            SqlCommand com = new SqlCommand("SELECT firstName, surname FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            SqlDataReader rd = com.ExecuteReader();
            string name = "";
            while (rd.Read())
            {
                name += rd["firstName"].ToString() + " ";
                name += rd["surname"].ToString();
            }
            con.Close();

            //---------------------------------------------
            //Get users placement information from database
            //---------------------------------------------
            SqlCommand com2 = new SqlCommand("SELECT startDate, endDate, placementID FROM apprenticePlacement WHERE apprenticeID=@appID ORDER BY startDate DESC", con);
            com2.Parameters.AddWithValue("@appID", apprenticeID);
            con.Open();
            SqlDataReader reader1 = com2.ExecuteReader();
            List<string> startDates = new List<string>();
            List<string> endDates = new List<string>();
            List<string> placementIDs = new List<string>();
            List<string> placementNames = new List<string>();

            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    string startDate = reader1.GetValue(0).ToString();
                    startDate = startDate.Remove(startDate.Length - 8);
                    startDates.Add(startDate.ToString());
                    string endDate = reader1.GetValue(1).ToString();
                    endDates.Add(endDate);
                    string placementID = reader1.GetValue(2).ToString();
                    placementIDs.Add(placementID);
                }
                reader1.NextResult();

            }
            con.Close();

            //Get placement names using placement ID
            for (int i = 0; i < placementIDs.Count; i++)
            {
                SqlCommand com4 = new SqlCommand("SELECT placementName FROM placement WHERE placementID=@placementID", con);
                com4.Parameters.AddWithValue("@placementID", placementIDs[i]);
                con.Open();
                placementNames.Add(com4.ExecuteScalar().ToString());
                con.Close();
            }


            //---------------------------------
            //Get objectives for each placement
            //---------------------------------
            if (flag == 0)
            {
                
                con.Open();
                for (int i = 0; i < placementIDs.Count; i++)
                {
                    SqlCommand cmd6 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@appID AND placementID=@placeID", con);
                    cmd6.Parameters.AddWithValue("@appID", apprenticeID);
                    cmd6.Parameters.AddWithValue("@placeID", placementIDs[i].ToString());
                    string appPlacementID = "";
                    try
                    {
                        appPlacementID = cmd6.ExecuteScalar().ToString();
                    }
                    catch (System.ArgumentOutOfRangeException)
                    {
                        //error
                        error.Visible = true;
                        error.Text = "An error has occured.";
                    }

                    SqlCommand cmd5 = new SqlCommand("SELECT objectives FROM placementInduction WHERE appPlacementID=@appPlacement", con);
                    cmd5.Parameters.AddWithValue("@appPlacement", appPlacementID);
                    string objs = "";
                    try
                    {
                        objs = cmd5.ExecuteScalar().ToString();
                    }
                    catch (System.ArgumentOutOfRangeException)
                    {
                        //error
                        error.Visible = true;
                        error.Text = "An error has occured.";
                    }

                    char[] separator = { '$' };
                    Int32 count = 5;
                    String[] strlist = null;
                    if (objs != string.Empty)
                    {
                        strlist = objs.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
                    }

                    for (int j = 0; j < strlist.Length; j++)
                    {
                        if (i == 0)
                        {
                            String num = (j + 1).ToString();
                            placement1Obj += num + ". ";
                            placement1Obj += strlist[j].ToString() + "\r\n";
                        }
                        else if (i == 1)
                        {
                            String num = (j + 1).ToString();
                            placement2Obj += num + ". ";
                            placement2Obj += strlist[j].ToString() + "\r\n";
                        }
                        else if (i == 2)
                        {
                            String num = (j + 1).ToString();
                            placement3Obj += num + ". ";
                            placement3Obj += strlist[j].ToString() + "\r\n";
                        }
                        else if (i == 3)
                        {
                            String num = (j + 1).ToString();
                            placement4Obj += num + ". ";
                            placement4Obj += strlist[j].ToString() + "\r\n";
                        }
                        else if (i == 4)
                        {
                            String num = (j + 1).ToString();
                            placement5Obj += num + ". ";
                            placement5Obj += strlist[j].ToString() + "\r\n";
                        }
                    }

                }
                con.Close();
                flag = 1;
            }

            foreach (Field myMergeField in wordDoc.Fields)
            {
                Range rngFieldCode = myMergeField.Code;

                String fieldText = rngFieldCode.Text;

                // getting the merge fields

                if (fieldText.StartsWith(" MERGEFIELD"))
                {

                    // THE TEXT COMES IN THE FORMAT OF
                    // MERGEFIELD  MyFieldName  \\* MERGEFORMAT
                    // THIS HAS TO BE EDITED TO GET ONLY THE FIELDNAME "MyFieldName"

                    Int32 endMerge = fieldText.IndexOf("\\");

                    Int32 fieldNameLength = fieldText.Length - endMerge;

                    String fieldName = fieldText.Substring(11, endMerge - 11);

                    // GIVES THE FIELDNAMES AS THE USER HAD ENTERED IN .dot FILE

                    fieldName = fieldName.Trim();

                    




                    //Replace the field
                    switch (fieldName)
                    {
                        case "Name":
                            myMergeField.Select();
                            wordApp.Selection.TypeText(name);
                            break;

                        case "startDate1":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(startDates[0].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "startDate2":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(startDates[1].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "startDate3":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(startDates[2].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "startDate4":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(startDates[3].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "startDate5":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(startDates[4].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "endDate2":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(endDates[1].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "endDate3":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(endDates[2].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "endDate4":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(endDates[3].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "endDate5":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(endDates[4].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "placement1":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(placementNames[0].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "placement2":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(placementNames[1].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "placement3":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(placementNames[2].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "placement4":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(placementNames[3].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "placement5":
                            myMergeField.Select();
                            try
                            {
                                wordApp.Selection.TypeText(placementNames[4].ToString());
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "scheme":
                            myMergeField.Select();
                            wordApp.Selection.TypeText(scheme.ToString());
                            break;

                        case "degreeYear":
                            myMergeField.Select();
                            if (degreeYear.SelectedValue != "0")
                            {
                                wordApp.Selection.TypeText(degreeYear.SelectedItem.ToString());
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "level4Year":
                            myMergeField.Select();
                            if (level4Year.SelectedValue != "0")
                            {
                                wordApp.Selection.TypeText(level4Year.SelectedItem.ToString());
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "degreeGrade":
                            myMergeField.Select();
                            if (degreeGrade.SelectedValue != "0")
                            {
                                wordApp.Selection.TypeText(degreeGrade.SelectedItem.ToString());
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "level4Grade":
                            myMergeField.Select();
                            if (level4Grade.SelectedValue != "0")
                            {
                                wordApp.Selection.TypeText(level4Grade.SelectedItem.ToString());
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "finalYearProj":
                            myMergeField.Select();
                            if (FYPName.Text != string.Empty) //If the user has entered text.
                            {
                                wordApp.Selection.TypeText(FYPName.Text);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "stretchObj":
                            myMergeField.Select();
                            if(stretchObj.Text != string.Empty)
                            {
                                wordApp.Selection.TypeText(stretchObj.Text);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "Obj1Place1":
                            myMergeField.Select();
                            if (placement1Obj != string.Empty)
                            {
                                wordApp.Selection.TypeText(placement1Obj);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "objPlace2":
                            myMergeField.Select();
                            if (placement2Obj != string.Empty)
                            {
                                wordApp.Selection.TypeText(placement2Obj);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "objPlace3":
                            myMergeField.Select();
                            if (placement3Obj != string.Empty)
                            {
                                wordApp.Selection.TypeText(placement3Obj);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "objPlace4":
                            myMergeField.Select();
                            if (placement4Obj != string.Empty)
                            {
                                wordApp.Selection.TypeText(placement4Obj);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "objPlace5":
                            myMergeField.Select();
                            if (placement5Obj != string.Empty)
                            {
                                wordApp.Selection.TypeText(placement5Obj);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "awards":
                            myMergeField.Select();
                            if (awards.Text != string.Empty)
                            {
                                wordApp.Selection.TypeText(awards.Text);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "programmingLanguages":
                            myMergeField.Select();
                            if (languages.Text != string.Empty)
                            {
                                wordApp.Selection.TypeText("Programming Languages: " + languages.Text);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;

                        case "tools":
                            myMergeField.Select();
                            if (tools.Text != string.Empty)
                            {
                                wordApp.Selection.TypeText(tools.Text);
                            }
                            else
                            {
                                wordApp.Selection.TypeText(" ");
                            }
                            break;
                    }
                }

            }
            wordDoc.SaveAs("myCV.doc");
            wordApp.Documents.Open("myCV.doc");
        
        }
    }
}