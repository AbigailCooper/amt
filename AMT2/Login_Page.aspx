﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true"  MasterPageFile="~/LoginMaster.Master" CodeBehind="Login_Page.aspx.cs" Inherits="AMT2.Login_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <div>
            <hr>
            <asp:Table class="table" ID="table" runat="server" Width="363px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label class="label" runat="server" Text="Username"></asp:Label>
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:TextBox ID="username" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label Class="label" runat="server" Text="Password"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:TextBox ID="password" type="password" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <hr />
            <asp:Button ID="button1" class="submit" type="submit" runat="server" Text="Login" OnClick="Login_Click" />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>

    <div class="container-signin">
            <p>Don't have an account? <a href="RegistrationPage.aspx"> Register</a>.</p> 
        </div>

</asp:Content>
