﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }
        }

        protected void button1_Click(object sender, EventArgs e)
        {
            //First check they are who they say they are.
            SqlCommand com = new SqlCommand("CUser", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            string encrypPass = Encryptpass(CurrentPass.Text);

            SqlParameter p1 = new SqlParameter("username", usernameS);
            SqlParameter p2 = new SqlParameter("password", encrypPass);
            com.Parameters.Add(p1);
            com.Parameters.Add(p2);
            con.Open();
            SqlDataReader rd = com.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                rd.Close();
                string newPassword = Encryptpass(newPass.Text);
                //Now update the password.
                SqlCommand update = new SqlCommand("UPDATE users SET password=@password WHERE username=@username", con);
                update.Parameters.AddWithValue("@password", newPassword);
                update.Parameters.AddWithValue("@username", usernameS);
                int i = update.ExecuteNonQuery();
                

                if(i != 1)
                {
                    Label1.Visible = true;
                    Label1.Text = "Password has been changed successfully.";
                }
            }
            else
            {
                Label1.Visible = true;
                Label1.Text = "Current Password Invalid.";            }
            con.Close();
        }

        public string Encryptpass(string password)
        {
            string msg = "";
            byte[] encode = new byte[password.Length];
            encode = System.Text.Encoding.UTF8.GetBytes(password);
            msg = Convert.ToBase64String(encode);
            return msg;
        }
    }
}