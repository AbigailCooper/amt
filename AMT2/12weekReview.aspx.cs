﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class _12weekReview : System.Web.UI.Page
    {
        string usernameS;
        string managerID;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //---------------------------
            //Fill the Apprentice Dropdown
            //---------------------------
            SqlCommand query = new SqlCommand("getUserID", con);
            query.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            query.Parameters.Add(p1);
            con.Open();
            managerID = query.ExecuteScalar().ToString(); // Get the ID of the manager
            con.Close();

            SqlCommand query2 = new SqlCommand("SELECT apprenticeID FROM apprenticePlacement WHERE managerID=@managerid", con);
            query2.Parameters.AddWithValue("@managerid", managerID); // Find those apprentices under the manager
            con.Open();
            
            List<string> apprenticesId = new List<string>();
            

            using (SqlDataReader reader = query2.ExecuteReader())
            {
                while (reader.Read())
                {
                    apprenticesId.Add(reader.GetString(0)); // Add all the apprentices under manager to a list.
                }
            }
            con.Close();

            if (!this.IsPostBack)
            {
                DataSet ds = new DataSet();
                for (int i=0; i < apprenticesId.Count; i++)
                {
                    SqlCommand com = new SqlCommand("SELECT username FROM users WHERE uID=@userID");
                    com.Parameters.AddWithValue("@userID", apprenticesId[i]);
                    using (com)
                    {
                        com.CommandType = CommandType.Text;
                        com.Connection = con;
                        using (SqlDataAdapter da = new SqlDataAdapter(com))
                        {
                            
                            da.Fill(ds);
                            apprentices.DataSource = ds.Tables[0];
                            apprentices.DataTextField = "username";
                            apprentices.DataValueField = "username";

                            apprentices.DataBind();
                        }
                    }
                }
                

                try
                {
                    if (apprentices.Items.FindByValue("-- Select Apprentice --").Value == null) { }
                }
                catch(NullReferenceException)
                {
                    apprentices.Items.Insert(0, new ListItem("--Select Apprentice--", "0"));
                }
            }

        }
        
        protected void submitReview_Click(object sender, EventArgs e)
        {
            if (apprentices.SelectedIndex == 0)
            {
                //Dont run as no apprentice is selected.
                errorMessage.Visible = true;
                errorMessage.Text = "Please select an apprentice.";
            }
            else
            {
                string HS = null; // Health and Safety.
                string KU = null; // Knowledge and Understanding
                string DP = null; // Design Processes
                string RM = null; // Responsibility and Management
                string CS = null; // Communication Skills
                string PC = null; // Professional Commitment
                string ED = null; // Equality and Diversity
                string At = null; // Attendance
                string GC = null; // General Comments

                const int EXCELLENT = 0;
                const int GOOD = 1;
                const int REQUIRESIMPROVEMENT = 2;
                const int UNDERPERFORMING = 3;

                String[] Rating = {
                "Excellent",
                "Good",
                "RequiresImprovement",
                "UnderPerforming"
            };


                foreach (TableCell cell in HSrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "HSExcellent": HS = Rating[EXCELLENT]; break;
                                case "HSGood": HS = Rating[GOOD]; break;
                                case "HSRequireI": HS = Rating[REQUIRESIMPROVEMENT]; break;
                                case "HSUnder": HS = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }


                }

                foreach (TableCell cell in KUrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {

                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "KUExcellent": KU = Rating[EXCELLENT]; break;
                                case "KUGood": KU = Rating[GOOD]; break;
                                case "KURequireI": KU = Rating[REQUIRESIMPROVEMENT]; break;
                                case "KUUnder": KU = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in DProw.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "DPExcellent": DP = Rating[EXCELLENT]; break;
                                case "DPGood": DP = Rating[GOOD]; break;
                                case "DPRequireI": DP = Rating[REQUIRESIMPROVEMENT]; break;
                                case "DPUnder": DP = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in RMrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "RMExcellent": RM = Rating[EXCELLENT]; break;
                                case "RMGood": RM = Rating[GOOD]; break;
                                case "RMRequireI": RM = Rating[REQUIRESIMPROVEMENT]; break;
                                case "RMUnder": RM = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in CSrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "CSExcellent": CS = Rating[EXCELLENT]; break;
                                case "CSGood": CS = Rating[GOOD]; break;
                                case "CSRequireI": CS = Rating[REQUIRESIMPROVEMENT]; break;
                                case "CSUnder": CS = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in PCrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "PCExcellent": PC = Rating[EXCELLENT]; break;
                                case "PCGood": PC = Rating[GOOD]; break;
                                case "PCRequireI": PC = Rating[REQUIRESIMPROVEMENT]; break;
                                case "PCUnder": PC = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in EDrow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "EDExcellent": ED = Rating[EXCELLENT]; break;
                                case "EDGood": ED = Rating[GOOD]; break;
                                case "EDRequireI": ED = Rating[REQUIRESIMPROVEMENT]; break;
                                case "EDUnder": ED = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                foreach (TableCell cell in Arow.Controls)
                {
                    foreach (RadioButton r in cell.Controls)
                    {
                        if (r.Checked)
                        {
                            switch (r.ID)
                            {
                                case "AExcellent": At = Rating[EXCELLENT]; break;
                                case "AGood": At = Rating[GOOD]; break;
                                case "ARequireI": At = Rating[REQUIRESIMPROVEMENT]; break;
                                case "AUnder": At = Rating[UNDERPERFORMING]; break;
                            }
                        }
                    }
                }

                GC = generalComments.Text;
                string apprenticeName = apprentices.SelectedValue;
                SqlCommand com2 = new SqlCommand("getUserID", con);
                com2.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("username", usernameS);
                com2.Parameters.Add(p1);
                con.Open();
                string apprenticeID = com2.ExecuteScalar().ToString();
                con.Close();

                //-----------------
                //Get AppPlacmentID
                //-----------------
                SqlCommand com = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE (apprenticeID=@apprenticeID) AND (managerID=@managerID)", con);
                com.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                com.Parameters.AddWithValue("@managerID", managerID);
                con.Open();
                string appPlacementID = com.ExecuteScalar().ToString();
                con.Close();

                //--------------------
                //Store data in the DB
                //--------------------

                SqlCommand query = new SqlCommand("INSERT INTO WeekReview values(@ReviewID, @appPlacementID, @HS, @KU, @DP, @RM, @CS, @PC, @ED, @AT, @GCM, @GCA, @date)", con);
                query.Parameters.AddWithValue("@ReviewID", UID());
                query.Parameters.AddWithValue("@appPlacementID", appPlacementID);
                query.Parameters.AddWithValue("@HS", HS);
                query.Parameters.AddWithValue("@KU", KU);
                query.Parameters.AddWithValue("@DP", DP);
                query.Parameters.AddWithValue("@RM", RM);
                query.Parameters.AddWithValue("@CS", CS);
                query.Parameters.AddWithValue("@PC", PC);
                query.Parameters.AddWithValue("@ED", ED);
                query.Parameters.AddWithValue("@AT", At);
                query.Parameters.AddWithValue("@GCM", GC);
                query.Parameters.AddWithValue("@GCA", "");
                query.Parameters.AddWithValue("@date", DateTime.Now);
                con.Open();
                int i = query.ExecuteNonQuery();
                con.Close();

                if (i == 1)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "Successfully saved.";
                }
                else
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "Error Ocurred.";
                }
            }
        }

        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        protected void apprentices_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set everything back to blank.
            lbl_Message.Visible = false;
            errorMessage.Visible = false;

            generalComments.Text = "";
            HSExcellent.Checked = false;
            HSGood.Checked = false;
            HSRequireI.Checked = false;
            HSUnder.Checked = false;

            KUExcellent.Checked = false;
            KUGood.Checked = false;
            KURequireI.Checked = false;
            KUUnder.Checked = false;

            DPExcellent.Checked = false;
            DPGood.Checked = false;
            DPRequireI.Checked = false;
            DPUnder.Checked = false;

            RMExcellent.Checked = false;
            RMGood.Checked = false;
            RMRequireI.Checked = false;
            RMUnder.Checked = false;

            CSExcellent.Checked = false;
            CSGood.Checked = false;
            CSRequireI.Checked = false;
            CSUnder.Checked = false;

            PCExcellent.Checked = false;
            PCGood.Checked = false;
            PCRequireI.Checked = false;
            PCUnder.Checked = false;

            EDExcellent.Checked = false;
            EDGood.Checked = false;
            EDRequireI.Checked = false;
            EDUnder.Checked = false;

            AExcellent.Checked = false;
            AGood.Checked = false;
            ARequireI.Checked = false;
            AUnder.Checked = false;
        }
    }
    
}
