﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class WorkPlacement : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //-----------------------------------------------------------------------
            //Check to see if user has entered a placement - show pop up to enter one
            //-----------------------------------------------------------------------

            //First get uID to lookup in placement table.
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            //-----------------------------------------
            //Only managers can see 12 week review form
            //-----------------------------------------

            bool manager = isUserManager(usernameS);
            if (manager)
            {
                week12.Visible = true;
                objectives.Visible = false; // Managers dont need to see objectives box.
                extraThroughout.Visible = false;
                extraFinal.Visible = false;
            }
            else
            {
                week12.Visible = false;
            }

            //Only EC can see all 12week review
            bool EC = isUserEC(usernameS);
            if(EC)
            {
                week12.Visible = true;
                objectives.Visible = false; // EC dont need to see objectives box
                extraThroughout.Visible = false;
                extraFinal.Visible = false;
            }
            else
            {
                week12.Visible = false;
            }

            // Apprentices have a read only state of 12 week reviews
            bool App = isUserApprentice(usernameS);
            if(App)
            {
                week12.Visible = true;
                objectives.Visible = true;
                extraThroughout.Visible = true;
                extraFinal.Visible = true;
            }
            else
            {
                week12.Visible = false;
                extraThroughout.Visible = false;
                objectives.Visible = false;
                extraFinal.Visible = false;
            }

            if (!manager && !EC && App)
            {

                //Use uID to see if user has a current placement assigned
                SqlCommand cmd2 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@uID", con);
                cmd2.Parameters.AddWithValue("@uID", apprenticeID);
                con.Open();
                try
                {
                    string appPID = cmd2.ExecuteScalar().ToString();
                } catch(NullReferenceException)
                {
                    addNewPlacement.Visible = true;
                    newPlaceBtn.Visible = true;
                    addNewPlacement.Text = "Please enter your current Placement";
                }
                con.Close();

            

                //---------------------------------------------------------------------------
                //See if user has been in their placement too long - i.e they need to update.
                //---------------------------------------------------------------------------

                //Use Apprentice ID
                SqlCommand cmd3 = new SqlCommand("SELECT endDate FROM apprenticePlacement WHERE startDate =( SELECT MAX(startDate) FROM apprenticePlacement WHERE apprenticeID=@apprenticeID)", con);
                cmd3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                con.Open();
                DateTime endDate = new DateTime();
                try
                {
                    endDate = DateTime.Parse(cmd3.ExecuteScalar().ToString());
                    if (endDate < DateTime.Now) // If the end date is in the past.
                    {
                        addNewPlacement.Visible = true;
                        newPlaceBtn.Visible = true;
                        addNewPlacement.Text = "Please enter your current Placement";
                    }

                }
                catch (NullReferenceException)
                {
                    addNewPlacement.Text = "Please Update placement information.";
                }
                con.Close();


                //--------------------
                // Fill in Objectives
                //--------------------
                SqlCommand cmd4 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@uID AND endDate=@endDate", con);
                cmd4.Parameters.AddWithValue("@uID", apprenticeID);
                cmd4.Parameters.AddWithValue("@endDate", endDate);
                con.Open();
                string appPlacement = null;
                try
                {
                    appPlacement = cmd4.ExecuteScalar().ToString();
                }
                catch (NullReferenceException)
                {
                    //ERROR
                }
                con.Close();
                SqlCommand cmd5 = new SqlCommand("SELECT objectives FROM placementInduction WHERE appPlacementID=@appPlacement", con);
                cmd5.Parameters.AddWithValue("@appPlacement", appPlacement);
                con.Open();
                string objs = cmd5.ExecuteScalar().ToString();

                char[] separator = { '$' };
                Int32 count = 5;

                String[] strlist = objs.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < strlist.Length; i++)
                {
                    String num = (i + 1).ToString();
                    objectives.Text += num + ". ";
                    objectives.Text += strlist[i].ToString() + "\r\n";
                }
                con.Close();

            }
        }

        protected void newPlaceBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddApprenticePlacement.aspx");
        }

        public bool isUserManager(string username)
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", username);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if(priv == "manager")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isUserEC(string username)
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", username);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "earlyCareers")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isUserApprentice(string username)
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", username);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if(priv == "apprentice")
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        protected void PlacementInduction_Click(object sender, EventArgs e)
        {
            bool manager = isUserManager(usernameS);
            bool apprentice = isUserApprentice(usernameS);
            bool EC = isUserEC(usernameS);

            if(apprentice)
            {
                Response.Redirect("PlacementInduction.aspx");
            }
            if(manager)
            {
                Response.Redirect("PlacementInductionManager.aspx");
            }
        }

        protected void extraThroughout_Click(object sender, EventArgs e)
        {
            bool apprentice = isUserApprentice(usernameS);
            if (apprentice)
            {
                Response.Redirect("UKSPEC.aspx");
            }
        }

        protected void extraFinal_Click(object sender, EventArgs e)
        {
            bool apprentice = isUserApprentice(usernameS);
            if (apprentice)
            {
                Response.Redirect("reviewObjectives.aspx");
            }
        }

        protected void week12_Click(object sender, EventArgs e)
        {
            if (isUserApprentice(usernameS))
            {
                Response.Redirect("12weekReviewApp.aspx");
            }
            else if (isUserEC(usernameS))
            {
                Response.Redirect("12weekReviewEC.aspx");
            }
            else if (isUserManager(usernameS))
            {
                Response.Redirect("12weekReview.aspx"); 
            }

        }
    }
}