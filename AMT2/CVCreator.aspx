﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CVCreator.aspx.cs" Inherits="AMT2.CVCreator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="EPA.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">CV Creator</h1>
    <br />
    <asp:Label runat="server" CssClass="description" Text="This page will help you by creating a good starting point for your CV. Fill in the information below and click run. If something isn't applicable to you, just leave the textbox blank."></asp:Label>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What grade did you achieve in your degree?"></asp:Label>
    <asp:DropDownList runat="server"  ID="degreeGrade">
        <asp:ListItem>-- Select --</asp:ListItem>
        <asp:ListItem>1st</asp:ListItem>
        <asp:ListItem>2:1</asp:ListItem>
        <asp:ListItem>2:2</asp:ListItem>
        <asp:ListItem>3rd</asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What year did you complete your degree?"></asp:Label>
    <asp:DropDownList runat="server"  ID="degreeYear"></asp:DropDownList>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What grade did you achieve in your Level 4?"></asp:Label>
    <asp:DropDownList runat="server"  id="level4Grade">
        <asp:ListItem>-- Select --</asp:ListItem>
        <asp:ListItem>Pass</asp:ListItem>
        <asp:ListItem>Fail</asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What year did you complete your Level 4?"></asp:Label>
    <asp:DropDownList runat="server"  ID="level4Year"></asp:DropDownList>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What was the title of your Final Year Project?"></asp:Label>
    <asp:TextBox runat="server" ID="FYPName" type="text"></asp:TextBox>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Have you completed any relevant Stretch Objectives? If so, what were they called?"></asp:Label>
    <asp:TextBox runat="server" ID="stretchObj" type="text"></asp:TextBox> 
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What achievements have you gained during the apprenticeship?"></asp:Label>
    <asp:TextBox runat="server" ID="awards" Type="text"></asp:TextBox>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="What relevant tools have you used throughout your apprenticeship? (Only give a few examples)"></asp:Label>
    <asp:TextBox runat="server" ID="tools" Type="text"></asp:TextBox>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" ID="languagesLabel" visible="false" Text="What programming languages have you used?"></asp:Label>
    <asp:TextBox runat="server" ID="languages" Visible="false" Type="text"></asp:TextBox>
    

    <asp:Button runat="server" ID="save" CssClass="submit" OnClick="save_Click" Text="Create" />
    <br />
    <asp:Label runat="server" ID="error" Visible="false"></asp:Label>
    <br />

</asp:Content>
