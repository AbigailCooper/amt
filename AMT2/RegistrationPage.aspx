﻿<%@ Page Title="Registration Page" Language="C#" MasterPageFile="~/Reg.Master" AutoEventWireup="true" CodeBehind="RegistrationPage.aspx.cs" Inherits="AMT2.RegistrationPage" Async="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <p>Please fill in the following form to register your account.</p>

        <hr>
        <asp:Label class="label" ID="firstName" runat="server" Text="First Name"></asp:Label>
        <br />
        <asp:TextBox Type="text" ID="nametext" runat="server" Width="200px"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="nametext" ErrorMessage="Please enter your name" />
        <br />
        <asp:Label class="label" ID="surname" runat="server" Text="Surname"></asp:Label>
        <br />
        <asp:TextBox type="text" ID="surnameText" runat="server" Width="200px"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="reqName2" ControlToValidate="surnameText" ErrorMessage="Please enter your name" />
        <br />

        <asp:Label class="label" ID="Label2" runat="server" Text="Apprentice or Manager?"></asp:Label>
        <br />
        <asp:DropDownList runat="server" CssClass="regDrop" ID="userType" AutoPostBack="true" EnableViewState="true" OnTextChanged="userType_SelectedIndexChanged" >
            <asp:ListItem Text="-- Select --"></asp:ListItem>
            <asp:ListItem Text="Apprentice" Value="Apprentice"></asp:ListItem>
            <asp:ListItem Text="Manager" Value="Manager"></asp:ListItem>
            <asp:ListItem Text="Early Careers" Value="EC"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator runat="server" ID="reqUser" ControlToValidate="userType" ErrorMessage="Please make a selection" />
        <br />
        <br />
            <br />
            <asp:Label  ID="apprenticesOnly" Class="label" runat="server" Text="Apprentices Only: "></asp:Label>
            <br /><br />
            <asp:Label  class="label" ID="scheme" runat="server" Text="Scheme"></asp:Label>
            <br />
            <asp:DropDownList  CssClass="regDrop" ID="schemeDropDown" runat="server" Enabled="true">
                <asp:ListItem Value="Aero">Aerospace Engineering</asp:ListItem>
                <asp:ListItem Value="Software">Software Engineering</asp:ListItem>
            </asp:DropDownList>
            <br /><br />
            <asp:Label  class="label" ID="startDate" runat="server" Text="Start Date"></asp:Label>
            <br />
            <asp:DropDownList  CssClass="regDrop" ID="startDateDropDown" runat="server">
                <asp:ListItem Value="2016">2016</asp:ListItem>
                <asp:ListItem Value="2017">2017</asp:ListItem>
                <asp:ListItem Value="2018">2018</asp:ListItem>
                <asp:ListItem Value="2019">2019</asp:ListItem>
            </asp:DropDownList>
        <br />
            <br />
            <asp:Label ID="managersOnlylbl" runat="server" Text="Managers Only: " Class="label"></asp:Label>
            <br /><br />
            <asp:Label runat="server" ID="placementLbl" Text="Placement Name" Class="label"></asp:Label>
            <br />
            <asp:DropDownList CssClass="regDrop" ID="placementsDropDown" DataTextField="placementName" runat="server"></asp:DropDownList>
        

        <br />
        <br />
        <asp:Label Class="label" ID="privilegelbl" runat="server" Text="Enter Registration code: "></asp:Label>
        <br />
        <asp:TextBox runat="server" ID="privilegeText" type="text"></asp:TextBox>

        <br />
        <br />
        <asp:Label class="label" ID="username" runat="server" Text="Username"></asp:Label>
        <br />
        <asp:TextBox ID="usernameText" type="text" runat="server"></asp:TextBox>
        <asp:Label ID="errorUsername" runat="server" Visible="false"></asp:Label>
        <asp:RequiredFieldValidator runat="server" ID="requsername" ControlToValidate="usernameText" ErrorMessage="Please enter a username" />
        <br />
        <asp:Label class="label" ID="password" runat="server" Text="Password"></asp:Label>
        <br />
        <asp:TextBox ID="passwordText" type="password" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="reqPassword" ControlToValidate="passwordText" ErrorMessage="Please enter a password" />
        <br />

        <asp:Label CssClass="label" runat="server" Text="Confirm Password"></asp:Label>
        <br />
        <asp:TextBox ID="confirmPassword" runat="server" type="password"></asp:TextBox>
        <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="passwordText" ControlToValidate="confirmPassword"
            ErrorMessage="The passwords do not match." Display="Dynamic" />

        <asp:Button class="submit" type="submit" runat="server" Text="Register" OnClick="submit_Click" />

        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>

        <div class="container-signin">
            <p>Already have an account? <a href="Login_Page.aspx">Sign in</a>.</p>
        </div>


</asp:Content>
