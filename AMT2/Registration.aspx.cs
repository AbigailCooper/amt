﻿using System;
using System.Text;
using System.Collections.Generic;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class Home : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\AMT2\AMT2\App_Data\Database.mdf;Integrated Security=True");


        protected void submit_Click(object sender, EventArgs e)
        {
            string encrypPass = Encryptpass(passwordText.Text);
            SqlCommand cmd = new SqlCommand("insert into apprentices values(@firstName, @surname, @username, @password, @scheme, @startDate)", con);
            cmd.Parameters.AddWithValue("@firstName", nametext.Text);
            cmd.Parameters.AddWithValue("@surname", surnameText.Text);
            cmd.Parameters.AddWithValue("@username", usernameText.Text);
            cmd.Parameters.AddWithValue("@password", encrypPass);
            cmd.Parameters.AddWithValue("@scheme", schemeDropDown.SelectedValue);
            cmd.Parameters.AddWithValue("@startDate", startDate.Text);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i != 0)
            {
                Label1.Text = "Registration Successful";
                //Direct to Login page
            }
            else
            {
                Label1.Text = "Error while Registering";
            }
        }

        public string Encryptpass(string password)
        {
            string msg = "";
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            msg = Convert.ToBase64String(encode);
            return msg;
        }
    }
}