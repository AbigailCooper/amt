﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class _12weekReviewEC : System.Web.UI.Page
    {
        string usernameS;
        string apprentice = "apprentice";
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //---------------------------
            //Fill the Apprentice Dropdown
            //---------------------------
            error.Visible = false;

            SqlCommand query2 = new SqlCommand("SELECT uID FROM users WHERE privelege=@apprentice", con);
            query2.Parameters.AddWithValue("@apprentice", apprentice);
            con.Open();

            List<string> apprenticesId = new List<string>();


            using (SqlDataReader reader = query2.ExecuteReader())
            {
                while (reader.Read())
                {
                    apprenticesId.Add(reader.GetString(0));
                }
            }
            con.Close();

            if (!this.IsPostBack)
            {
                DataSet ds = new DataSet();
                for (int i = 0; i < apprenticesId.Count; i++)
                {
                    SqlCommand com = new SqlCommand("SELECT username FROM users WHERE uID=@userID");
                    com.Parameters.AddWithValue("@userID", apprenticesId[i]);
                    using (com)
                    {
                        com.CommandType = CommandType.Text;
                        com.Connection = con;
                        using (SqlDataAdapter da = new SqlDataAdapter(com))
                        {

                            da.Fill(ds);
                            apprentices.DataSource = ds.Tables[0];
                            apprentices.DataTextField = "username";
                            apprentices.DataValueField = "username";

                            apprentices.DataBind();
                        }
                    }
                }
                apprentices.Items.Insert(0, new ListItem("--Select Apprentice--", ""));
                apprentices.Items[0].Attributes["disabled"] = "disabled";
            }
        }

        private void updateTable()
        {
            //Get data from database
            
            string apprentice = apprentices.SelectedValue.ToString();
            string appID = getAppID(apprentice);
            // Get the appPlacmentID
            SqlCommand com2 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@uID", con);
            com2.Parameters.AddWithValue("@uID", appID);

            List<string> list = new List<string>();
            con.Open();
            SqlDataReader reader = com2.ExecuteReader();
            while(reader.Read())
            {
                list.Add(reader.GetString(0));

            }
            string[] array = list.ToArray();
            if (array.Length > 0)
            {


                string query = "SELECT healthAndSafety, knowledgeAndUnderstanding, designProcesses, responsibilityManagement, commsSkills, profCommitment, equalityDiversity, attendance, dateOfReview FROM WeekReview WHERE ";
                for (int i = 0; i < array.Length; i++)
                {
                    if (i != array.Length - 1)
                    {
                        query += "appPlacementID='" + list[i] + "' OR ";
                    }
                    else
                    {
                        query += "appPlacementID='" + list[i] + "'";
                    }
                }
                query += " ORDER BY dateOfReview DESC";
                con.Close();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    apprenticeReview.DataSource = dt;
                    apprenticeReview.DataBind();
                }
                con.Close();
            }
            else
            {
            }
            con.Close();
        }

        public string getAppID(string apprentice)
        {
            // Get uID of selected apprentice
            if(apprentices.SelectedIndex == 0)
            {
                return "";
            }
            else
            {
                SqlCommand com = new SqlCommand("getUserID", con);
                com.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("username", apprentice);
                com.Parameters.Add(p1);
                con.Open();
                string uID = com.ExecuteScalar().ToString();
                con.Close();
                return uID;
            }
            
        }

        protected void apprentices_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reset values.
            apprenticeReview.DataSource = null;
            apprenticeReview.DataBind();
            error.Visible = false;
            errorMessage.Visible = false;

            updateTable();
        }

        public string getAppPlacementID(string uID)
        {
            SqlCommand com = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE startDate=(SELECT max(startDate) FROM apprenticePlacement WHERE apprenticeID=@uID)", con);
            com.Parameters.AddWithValue("@uID", uID);
            con.Open();
            string appPlacementID = com.ExecuteScalar().ToString();
            con.Close();
            return appPlacementID;
        }

        protected void SaveComments_Click(object sender, EventArgs e)
        {
            if(apprentices.SelectedIndex == 0)
            {
                //Dont run as no apprentice is selected.
                errorMessage.Visible = true;
                errorMessage.Text = "Please select an apprentice.";
            }
            else
            {
                string comments = generalComsEC.Text;
                string appID = getAppID(apprentices.SelectedValue.ToString());
                string appPlacementID = getAppPlacementID(appID);

                SqlCommand com = new SqlCommand("UPDATE weekReview SET generalCommentsAssessor=@comments WHERE dateOfReview=(SELECT max(dateOfReview) FROM WeekReview WHERE appPlacementID=@appPlacementID)", con);
                com.Parameters.AddWithValue("@comments", comments);
                com.Parameters.AddWithValue("@appPlacementID", appPlacementID);
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();

                if (i != 1)
                {
                    error.Visible = true;
                    error.Text = "Error, unable to save to database";
                }
                else
                {
                    error.Visible = true;
                    error.Text = "Saved Successfully";
                }
            }
            
        }
    }
}