﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class _12weekReviewApp : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //------------------------------------
            //Update gridview with 12 week reviews
            //------------------------------------
            //Get data from database
            string appID = getAppID(usernameS);

            // Get the appPlacmentID
            SqlCommand com2 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@uID", con);
            com2.Parameters.AddWithValue("@uID", appID);

            List<string> list = new List<string>();
            con.Open();
            SqlDataReader reader = com2.ExecuteReader();
            while (reader.Read())
            {
                list.Add(reader.GetString(0));

            }
            string[] array = list.ToArray();
            if (array.Length > 0)
            {


                string query = "SELECT healthAndSafety, knowledgeAndUnderstanding, designProcesses, responsibilityManagement, commsSkills, profCommitment, equalityDiversity, attendance, generalCommentsManager, generalCommentsAssessor, dateOfReview FROM WeekReview WHERE ";
                for (int i = 0; i < array.Length; i++)
                {
                    if (i != array.Length - 1)
                    {
                        query += "appPlacementID='" + list[i] + "' OR ";
                    }
                    else
                    {
                        query += "appPlacementID='" + list[i] + "'";
                    }
                }
                query += " ORDER BY dateOfReview DESC";
                con.Close();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    apprenticeReview.DataSource = dt;
                    apprenticeReview.DataBind();
                }
                con.Close();
            }
            else
            {
                //Error - nothing in array so do not run!!
            }
        }

        public string getAppID(string username)
        {
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", username);
            com.Parameters.Add(p1);
            con.Open();
            string uID = com.ExecuteScalar().ToString();
            con.Close();
            return uID;
        }
    }
}