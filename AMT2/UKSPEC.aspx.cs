﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace AMT2
{
    public partial class UKSPEC : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }

            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //----------------------
            //Get data from database
            //----------------------
            String query = "SELECT date, location, eventName, eventLead, timeSpent, BehavioursDeveloped FROM ukspec ORDER BY date ASC";
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                UKSPECdata.DataSource = dt;
                UKSPECdata.DataBind();
            }
        }

        protected void Save_UKSPEC_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string uniqueID = UID();
                string apprenticeID = getApprenticeID(usernameS);
                if (apprenticeID == null)
                {
                    //throw an exception here!
                }
                string behaviours = getBehavioursDev();

                SqlCommand cmd = new SqlCommand("insert into ukspec values(@Id, @apprenticeID, @date, @location, @eventName, @eventLead, @timeSpent, @behavioursDeveloped)", con);

                cmd.Parameters.AddWithValue("@Id", uniqueID);
                cmd.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                cmd.Parameters.AddWithValue("@date", EnrichmentCalendar.SelectedDate);
                cmd.Parameters.AddWithValue("@location", Location.Text);
                cmd.Parameters.AddWithValue("@eventName", CourseName.Text);
                cmd.Parameters.AddWithValue("@eventLead", EventLead.Text);
                cmd.Parameters.AddWithValue("@timeSpent", float.Parse(TimeSpent.Text));
                cmd.Parameters.AddWithValue("@behavioursDeveloped", behaviours);
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                FormAddUKSPEC.Visible = false;
                labelSaved.Text = "Saved!";
                Response.Redirect("UKSPEC.aspx"); // Refresh the page to refresh the table.
            }
        }

        //-------------------------------------------------------
        // Put the selected behaviours in a comma separated list.
        //-------------------------------------------------------
        private string getBehavioursDev()
        {
            StringBuilder behaviours = new StringBuilder();
            foreach(ListItem li in Behaviours.Items)
            {
                if(li.Selected)
                {
                    behaviours.Append(li).Append(", ");
                }
            }
            string behavioursDeveloped = behaviours.ToString().TrimEnd(' ').TrimEnd(',');
            return behavioursDeveloped;
        }

        //-----------------------------------------------------------------------------
        // Retrieve the apprentice ID from the apprentice table using the session name.
        //-----------------------------------------------------------------------------
        public string getApprenticeID(string username)
        {
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", username);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            return apprenticeID;
        }

        //----------------------
        // Make the form visable
        //----------------------
        protected void btnShow_Click(object sender, EventArgs e)
        {
            FormAddUKSPEC.Visible = true;
        }

        //--------------------
        // Create a unique ID.
        //--------------------
        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        protected void Unnamed_Click(object sender, ImageClickEventArgs e)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }
}