﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AddApprenticePlacement.aspx.cs" Inherits="AMT2.AddApprenticePlacement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />

    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />

    <div>
        <asp:Table runat="server" ID="addAppPlaceTbl" Class="table">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" class="label">Placement Name:</asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList runat="server" CssClass="dropdown" ID="PlacementDrop">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label runat="server" CssClass="label">Start Date: </asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Calendar  ID="EnrichmentCalendar" runat="server">
                        <DayHeaderStyle  BackColor="#cccccc"/>
                        <SelectedDayStyle BackColor="OrangeRed" ForeColor="White" />
                    </asp:Calendar>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br />
        <asp:Button runat="server" class="submit" type="submit" Text="Save" OnClick="SaveAppPlace_Click" /> <br />
        <asp:Label class="error" id="errorLabel" runat="server" Visible="false"></asp:Label>
    </div>

</asp:Content>
