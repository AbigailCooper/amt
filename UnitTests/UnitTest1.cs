﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AMT2;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void checkUsernameTest()
        {
            //Username that already exists. Should return true.
            RegistrationPage registration = new RegistrationPage();
            string username = "abigail.cooper"; 

            bool answer = registration.checkUsername(username);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void checkUsernameTest2()
        {
            //Username does not exist. Should return false.
            RegistrationPage registration = new RegistrationPage();
            string username = "testUsername";

            bool answer = registration.checkUsername(username);

            Assert.AreEqual(false, answer);
        }

        [TestMethod]
        public void checkGetPrivilege1()
        {
            //apprenticeLog01 should return apprentice
            RegistrationPage registration = new RegistrationPage();
            string privilege = "apprenticeLog01";

            string answer = registration.getPrivilege(privilege);

            Assert.AreEqual("apprentice", answer);
        }

        [TestMethod]
        public void checkGetPrivilege2()
        {
            //managerApproved_01 should return manager
            RegistrationPage registration = new RegistrationPage();
            string privilege = "managerApproved_01";

            string answer = registration.getPrivilege(privilege);

            Assert.AreEqual("manager", answer);
        }

        [TestMethod]
        public void checkGetPrivilege3()
        {
            //EarlyCareers should return earlyCareers
            RegistrationPage registration = new RegistrationPage();
            string privilege = "EarlyCareers";

            string answer = registration.getPrivilege(privilege);

            Assert.AreEqual("earlyCareers", answer);
        }

        [TestMethod]
        public void checkGetApprenticeID()
        {
            //The correct ID should be pulled from the database.
            UKSPEC uKSPEC = new UKSPEC();
            string username = "abigail.cooper";

            string answer = uKSPEC.getApprenticeID(username);

            Assert.AreEqual("f5dfaa5e-69e7-4bbb-ae6f-3fe2ddff0d9c", answer);
        }

        [TestMethod]
        public void checkIsUserManager1()
        {
            //abigail.cooper is an apprentice so should return false.
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "abigail.cooper";

            bool answer = workPlacement.isUserManager(username);

            Assert.AreEqual(false, answer);
        }

        [TestMethod]
        public void checkIsUserManager2()
        {
            //nick.taylor is a manager so should return true.
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "nick.taylor";

            bool answer = workPlacement.isUserManager(username);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void checkIsUserEC()
        {
            //amie.tattam is a member of EC so should return true.
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "amie.tattam";

            bool answer = workPlacement.isUserEC(username);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void checkIsUserEC2()
        {
            //nick.taylor is a manager so should return false.
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "nick.taylor";

            bool answer = workPlacement.isUserEC(username);

            Assert.AreEqual(false, answer);
        }

        [TestMethod]
        public void checkIsUserApprentice1()
        {
            //abigail.cooper is an apprentice so should return true
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "abigail.cooper";

            bool answer = workPlacement.isUserApprentice(username);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void checkIsUserApprentice2()
        {
            //amie.tattam is a member of EC so should return false
            WorkPlacement workPlacement = new WorkPlacement();
            string username = "amie.tattam";

            bool answer = workPlacement.isUserApprentice(username);

            Assert.AreEqual(false, answer);
        }

        [TestMethod]
        public void checkGetAppPlacementID()
        {
            // uID: f5dfaa5e-69e7-4bbb-ae6f-3fe2ddff0d9c should give placmentID: 0a3965a8-25b1-4a3a-b7ce-2144d34b6827
            _12weekReviewEC _12WeekReviewEC = new _12weekReviewEC();
            string apprenticeID = "f5dfaa5e-69e7-4bbb-ae6f-3fe2ddff0d9c";

            string placementID = _12WeekReviewEC.getAppPlacementID(apprenticeID);

            Assert.AreEqual("0a3965a8-25b1-4a3a-b7ce-2144d34b6827", placementID);
        }

        [TestMethod]
        public void checkUID()
        {
            //Run method twice and show they are not the same
            PlacementInduction placementInduction = new PlacementInduction();

            string uID1 = placementInduction.UID();
            string uID2 = placementInduction.UID();

            Assert.AreNotEqual(uID1, uID2);
        }

        [TestMethod]
        public void checkGetPlacementInductionID()
        {
            //for abigail.cooper the induction ID should be: 15998f66-11b5-447e-9f6c-647058facfa3
            PlacementInduction placementInduction = new PlacementInduction();
            string username = "abigail.cooper";

            string placementInductionID = placementInduction.getPlacementInductionID(username);

            Assert.AreEqual("15998f66-11b5-447e-9f6c-647058facfa3", placementInductionID);



        }

        [TestMethod]
        public void checkPlaceIndIsUserApprentice1()
        {
            //james.kay is an apprentice so should return true
            PlacementInduction placementInduction = new PlacementInduction();
            string username = "james.kay";

            bool answer = placementInduction.isUserApprentice(username);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void checkPlaceIndIsUserApprentice2()
        {
            //james.kay is a  so should return false
            PlacementInduction placementInduction = new PlacementInduction();
            string username = "jack.bishop01";

            bool answer = placementInduction.isUserApprentice(username);

            Assert.AreEqual(false, answer);
        }

        [TestMethod]
        public void checkisUserData()
        {
            //abigail.cooper does have data so returns true
            recordKQ recordKQ = new recordKQ();
            string userID = "f5dfaa5e-69e7-4bbb-ae6f-3fe2ddff0d9c";

            bool answer = recordKQ.isUserData(userID);

            Assert.AreEqual(true, answer);
        }

        [TestMethod]
        public void CheckGetAppID()
        {
            //james.kay should have the id:e572113e-2011-48fa-af66-c28db5bbc35c
            _12weekReviewApp _12WeekReviewApp = new _12weekReviewApp();
            string username = "james.kay";

            string apprenticeID = _12WeekReviewApp.getAppID(username);

            Assert.AreEqual("e572113e-2011-48fa-af66-c28db5bbc35c", apprenticeID);
        }

        [TestMethod]
        public void CheckGetPlacementID1()
        {
            //csg should have the placementID: 002
            AddApprenticePlacement apprenticePlacement = new AddApprenticePlacement();
            string placementName = "002";

            string placementID = apprenticePlacement.getPlacementID(placementName);

            Assert.AreEqual("002", placementID);
        }

        [TestMethod]
        public void CheckGetPlacementID2()
        {
            //FTI should have the placementID: 006 --> the function uses the selectedVALUE from the dropdown.
            AddApprenticePlacement apprenticePlacement = new AddApprenticePlacement();
            string placementName = "006";

            string placementID = apprenticePlacement.getPlacementID(placementName);

            Assert.AreEqual("006", placementID);
        }

        [TestMethod]
        public void CheckEncryptpass()
        {
            //Check the same string creates the same encryption when done twice
            ChangePassword changePassword = new ChangePassword();
            string password1 = "helloWorld";
            string password2 = "helloWorld";

            string encryp1 = changePassword.Encryptpass(password1);
            string encryp2 = changePassword.Encryptpass(password2);

            Assert.AreEqual(encryp1, encryp2);
        }

        [TestMethod]
        public void CheckEncryptpass2()
        {
            //Check different strings create the different encryptions
            ChangePassword changePassword = new ChangePassword();
            string password1 = "theSunIsYe11ow";
            string password2 = "grassIsGr33n";

            string encryp1 = changePassword.Encryptpass(password1);
            string encryp2 = changePassword.Encryptpass(password2);

            Assert.AreNotEqual(encryp1, encryp2);
        }

        [TestMethod]
        public void CheckJobDescriptionUID()
        {
            //Run method twice and show they are not the same
            JobDescription jobDescription = new JobDescription();

            string uID1 = jobDescription.UID();
            string uID2 = jobDescription.UID();

            Assert.AreNotEqual(uID1, uID2);
        }

        [TestMethod]
        public void CheckGetPlacementInductionID()
        {
            //15998f66-11b5-447e-9f6c-647058facfa3 is the placementInduction ID for abigail.cooper
            reviewObjectives reviewObjectives = new reviewObjectives();
            string username ="abigail.cooper";

            string placementInductionID = reviewObjectives.getPlacementInductionID(username);

            Assert.AreEqual("15998f66-11b5-447e-9f6c-647058facfa3", placementInductionID);
        }
    }
}
