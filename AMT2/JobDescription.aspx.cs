﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class JobDescription : System.Web.UI.Page
    {
        string usernameS;
        string apprenticeID;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //----------
            //Get userID
            //----------
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            
        }

        protected void save_Click(object sender, EventArgs e)
        {
            string jobDescription = jobDesc.Text;

            //Check if apprentice is already in database
            try
            {
                SqlCommand com = new SqlCommand("SELECT Id FROM EPA WHERE apprenticeID=@appID", con);
                com.Parameters.AddWithValue("@appID", apprenticeID);
                con.Open();
                string ID = com.ExecuteScalar().ToString();
                con.Close();

                //In database so UPDATE
                SqlCommand com2 = new SqlCommand("UPDATE EPA SET jobDescription=@jobDesc WHERE apprenticeID=@appID", con);
                com2.Parameters.AddWithValue("@jobDesc", jobDescription);
                com2.Parameters.AddWithValue("@appID", apprenticeID);
                con.Open();
                int i = com2.ExecuteNonQuery();
                con.Close();

                if(i != 1)
                {
                    error.Visible = true;
                    error.Text = "Unable to save to the database.";
                }
                else if (i == 1)
                {
                    error.Visible = true;
                    error.Text = "Saved.";
                }
            }
            catch (NullReferenceException)
            {
                //Not in database so INSERT
                SqlCommand com3 = new SqlCommand("INSERT INTO EPA VALUES(@ID, @jobDesc, @short, @medium, @long, @appID, @TR1, @TR2, @TR3)", con);
                com3.Parameters.AddWithValue("@ID", UID());
                com3.Parameters.AddWithValue("@jobDesc", jobDescription);
                com3.Parameters.AddWithValue("@short", " ");
                com3.Parameters.AddWithValue("@medium", " ");
                com3.Parameters.AddWithValue("@long", " ");
                com3.Parameters.AddWithValue("@TR1", 0);
                com3.Parameters.AddWithValue("@TR2", 0);
                com3.Parameters.AddWithValue("@TR3", 0);
                com3.Parameters.AddWithValue("@appID", apprenticeID);
                //con.Open();
                int i = com3.ExecuteNonQuery();
                con.Close();

                if(i != 1)
                {
                    error.Visible = true;
                    error.Text = "Unable to save to the database.";
                }
                else if(i==1)
                {
                    error.Visible = true;
                    error.Text = "Saved.";
                }
            }

        }

        public string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }
    }
}