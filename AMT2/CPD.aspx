﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CPD.aspx.cs" Inherits="AMT2.CPD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="EPA.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Continuing Personal Developement</h1>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Create goals for you development going into the future, after your apprenticeship."></asp:Label>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Short term goal (The next few months)"></asp:Label>
    <asp:TextBox runat="server" ID="shortGoal" Width="400px"></asp:TextBox>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Medium term goal (The next year or so)"></asp:Label>
    <asp:TextBox runat="server" ID="mediumGoal" Width="400px"></asp:TextBox>
    <br />
    <br />
    <asp:Label runat="server" CssClass="label" Text="Long term goal (The next few years)"></asp:Label>
    <asp:TextBox runat="server" ID="longGoal" Width="400px"></asp:TextBox>
    <br />
    <br />
    <asp:Button runat="server" ID="save" CssClass="submit" Text="Save" OnClick="save_Click" />
    <br />
    <asp:Label runat="server" CssClass="error" ID="error" Visible="false" style="top:650px; left:500px"></asp:Label>
    <br />

</asp:Content>
