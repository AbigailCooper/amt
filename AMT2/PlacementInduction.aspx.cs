﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class PlacementInduction : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            updateTable();
        }

        protected void Save_objectives(object sender, EventArgs e)
        {
            string uniqueID = UID();
            string appPlacementID = getApprenticePlaceID(usernameS);
            //Get values from checkboxes.
            int fire = fireExits.Checked ? 1 : 0;
            int marshalls = fireMarshall.Checked ? 1 : 0;
            int firstAid = firstAiders.Checked ? 1 : 0;
            int SHEck = SHE.Checked ? 1 : 0;
            int meet = meetTeam.Checked ? 1 : 0;
            string objs = collateObjs();

            SqlCommand cmd = new SqlCommand("insert into placementInduction values(@Id, @appPlacementID, @fireAndAssembly, @fireMarshalls, @firstAiders, @SHE, @meetTeam, @objectives)", con);

            cmd.Parameters.AddWithValue("@Id", uniqueID);
            cmd.Parameters.AddWithValue("@appPlacementID", appPlacementID);
            cmd.Parameters.AddWithValue("@fireAndAssembly", fire);
            cmd.Parameters.AddWithValue("@fireMarshalls", marshalls);
            cmd.Parameters.AddWithValue("@firstAiders", firstAid);
            cmd.Parameters.AddWithValue("@SHE", SHEck);
            cmd.Parameters.AddWithValue("@meetTeam", meet);
            cmd.Parameters.AddWithValue("@objectives", objs);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        private string collateObjs()
        {
            string objectives = "";

            objectives += obj1.Text;
            objectives += " $ ";
            objectives += obj2.Text;
            objectives += " $ ";
            objectives += obj3.Text;
            objectives += " $ ";
            objectives += obj4.Text;
            objectives += " $ ";
            objectives += obj5.Text;

            return objectives;
        }

        // Create a unique ID.
        public string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        // Retrieve the apprentice placement ID from the appPlacement table using the session name.
        public string getApprenticePlaceID(string username)
        {
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", username);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            SqlCommand cmd3 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@apprenticeID", con);
            cmd3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            string appPlacementID = cmd3.ExecuteScalar().ToString();
            con.Close();

            return appPlacementID;
        }

        public string getPlacementInductionID(string username)
        {
            string appPlacementID = getApprenticePlaceID(username);
            SqlCommand cmd = new SqlCommand("SELECT id FROM placementInduction WHERE appPlacementID=@appPlacementID", con);
            cmd.Parameters.AddWithValue("@appPlacementID", appPlacementID);
            con.Open();
            string placementInductionID = cmd.ExecuteScalar().ToString();
            con.Close();

            return placementInductionID;
        }

        protected void SaveUpdate_Click(object sender, EventArgs e)
        {
            string ID = UID();
            DateTime date = DateTime.Now;
            string placementInductionID = getPlacementInductionID(usernameS);
            SqlCommand cmd = new SqlCommand("INSERT INTO notes VALUES(@noteID, @note, @entryDate, @username, @placementInductionID)", con);
            cmd.Parameters.AddWithValue("@noteID", ID);
            cmd.Parameters.AddWithValue("@note", Update.Text);
            cmd.Parameters.AddWithValue("@entryDate", date);
            cmd.Parameters.AddWithValue("@username", usernameS);
            cmd.Parameters.AddWithValue("@placementInductionID", placementInductionID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            Update.Text = ""; // Clear the text box.
            Response.Redirect("PlacementInduction.aspx"); //Refresh the page.
        }

        private void updateTable()
        {
            //Get data from database

            string placementInductionID = getPlacementInductionID(usernameS);

            String query = "SELECT note, username, entryDate FROM Notes WHERE placementInductionID='" + placementInductionID + "' ORDER BY entryDate DESC";
            
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                objUpdates.DataSource = dt;
                objUpdates.DataBind();
            }
        }

        public bool isUserApprentice(string username)
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", username);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "apprentice")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}