﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Word;
using System.Data.SqlClient;

namespace AMT2
{
    public partial class EOP : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //Managers and EC have no reason to see this information. All EPA documents get sent via email from apprentice to EC.
            //This is purely to guide apprentices and help them keep track.
            if(isUserEC() || isUserManager())
            {
                cvCreator.Visible      = false;
                PIRF.Visible           = false;
                jobDescription.Visible = false;
                taskReports.Visible    = false;
                CPD.Visible            = false;
            }
            
        }

        protected void cvCreator_Click(object sender, EventArgs e)
        {
            Response.Redirect("CVCreator.aspx");
        }

        protected void jobDescription_Click(object sender, EventArgs e)
        {
            Response.Redirect("JobDescription.aspx");
        }

        protected void CPD_Click(object sender, EventArgs e)
        {
            Response.Redirect("CPD.aspx");
        }

        protected void taskReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("TaskReports.aspx");
        }

        protected void PIRF_Click(object sender, EventArgs e)
        {
            Response.Redirect("PIRF.aspx");
        }

        // Checks if the user is a manager.
        protected bool isUserManager()
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "manager")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Checks if the user is Early Careers.
        protected bool isUserEC()
        {
            SqlCommand com = new SqlCommand("SELECT privelege FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", usernameS);
            con.Open();
            string priv = com.ExecuteScalar().ToString();
            con.Close();
            if (priv == "earlyCareers")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
}