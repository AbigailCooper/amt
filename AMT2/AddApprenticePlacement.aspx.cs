﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class AddApprenticePlacement : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //---------------------------
            //Fill the Placement Dropdown
            //---------------------------
            if (!this.IsPostBack)
            {
                using (SqlCommand com = new SqlCommand("SELECT * FROM placement"))
                {
                    com.CommandType = CommandType.Text;
                    com.Connection = con;
                    using (SqlDataAdapter da = new SqlDataAdapter(com))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        PlacementDrop.DataSource = ds.Tables[0];
                        PlacementDrop.DataTextField = "placementName";
                        PlacementDrop.DataValueField = "placementID";

                        PlacementDrop.DataBind();
                    }
                }
            }
            PlacementDrop.Items.Insert(0, new ListItem("--Select Placement--", "0"));
                
        }
        
        //----------------------------------------
        // Save the Placement data to the database
        //----------------------------------------
        protected void SaveAppPlace_Click(object sender, EventArgs e)
        {
            string uniqueID = UID();
            string userID   = getApprenticeID();
            string placementID = getPlacementID(PlacementDrop.SelectedValue);
            string managerID = getManagerID(placementID);
            DateTime endDate = getEndDate(userID);

            SqlCommand cmd2 = new SqlCommand("INSERT into apprenticePlacement values(@appPlacementID, @apprenticeID, @placementID, @managerID, @startDate, @endDate)", con);

            cmd2.Parameters.AddWithValue("@appPlacementID", uniqueID);
            cmd2.Parameters.AddWithValue("@apprenticeID", userID);
            cmd2.Parameters.AddWithValue("@placementID", placementID);
            cmd2.Parameters.AddWithValue("@managerID", managerID);
            cmd2.Parameters.AddWithValue("@startDate", EnrichmentCalendar.SelectedDate);
            cmd2.Parameters.AddWithValue("@endDate", endDate);
            con.Open();
            int i = cmd2.ExecuteNonQuery();
            con.Close();

            if(i != 0)
            {
                Response.Redirect("WorkPlacement.aspx");
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Unable to save. Please check the connection and try again.";
            }
        }

        //--------------------
        // Create a unique ID.
        //--------------------
        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        //------------------------------------
        // Get ApprenticeID using the username
        //------------------------------------
        private string getApprenticeID()
        {
            // Retrieve the apprentice ID from the apprentice table using the session name.

            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            string apprenticeID = com.ExecuteScalar().ToString();
            con.Close();
            return apprenticeID;
        }

        //------------------------------------
        // Get PlacementID using placementName
        //------------------------------------
        public string getPlacementID(string placement)
        {
            SqlCommand cmd3 = new SqlCommand("SELECT placementID FROM placement WHERE placementID=@placementName", con);
            cmd3.Parameters.AddWithValue("@placementName", placement);
            con.Open();
            string placementID = cmd3.ExecuteScalar().ToString();
            con.Close();
            return placementID;
        }

        //--------------------------------
        // Get managerID using placementID
        //--------------------------------
        private string getManagerID(string placementID)
        {
            string managerID;
            SqlCommand cmd4 = new SqlCommand("SELECT uID FROM managers WHERE placementID=@placementID", con);
            cmd4.Parameters.AddWithValue("@placementID", placementID);
            con.Open();
            try
            {
                 managerID = cmd4.ExecuteScalar().ToString();
            }
            catch
            {
                errorLabel.Text = "This placement does not have a manager assigned. Please inform your manager to contact EC.";
                managerID = "null";
            }
            
            con.Close();
            return managerID;
        }

        //--------------------------------------------------------------------------------
        // Get the End date based on the start date, depending on what scheme they are on.
        //--------------------------------------------------------------------------------
        private DateTime getEndDate(string userID)
        {
            DateTime startDate = EnrichmentCalendar.SelectedDate;
            DateTime endDate;

            SqlCommand cmd5 = new SqlCommand("SELECT scheme FROM apprentices WHERE uID=@userID", con);
            cmd5.Parameters.AddWithValue("@userID", userID);
            con.Open();
            string scheme = cmd5.ExecuteScalar().ToString();
            con.Close();

            if(scheme == "Software")
            {
                endDate = startDate.AddMonths(7); //Software have 7 month long placements
            }
            else //Aerospace
            {
                endDate = startDate.AddMonths(5); //Aero have 5 month long placements
            }
            
            return endDate;
        }
    }
}