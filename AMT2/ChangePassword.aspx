﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="AMT2.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />

    <div>
        <hr>
        <asp:Table class="table" ID="table" runat="server" Width="363px">
            <asp:TableRow>
                <asp:TableCell>
                        <asp:Label class="label" runat="server" Text="Current Password"></asp:Label>
                        
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:TextBox ID="CurrentPass" TextMode="Password" runat="server"></asp:TextBox> <br />
                    <asp:RequiredFieldValidator ID="RFV1" runat="server" ControlToValidate="CurrentPass" ErrorMessage="Please enter current password."></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                        <asp:Label Class="label" runat="server" Text="New Password"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:TextBox ID="newPass" type="password" runat="server"></asp:TextBox> <br />
                    <asp:RequiredFieldValidator ID="RFV2" runat="server" ControlToValidate="newPass" ErrorMessage="Please enter new password."></asp:RequiredFieldValidator> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label Class="label" runat="server" Text="Confirm New Password"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:TextBox ID="confirmNew" type="password" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RFV3" runat="server" ControlToValidate="confirmNew" ErrorMessage="Please confirm new password."></asp:RequiredFieldValidator> 
                    <asp:CompareValidator ID="CV1" runat="server" ControlToCompare="newPass" ControlToValidate="confirmNew" ErrorMessage="Password Mismatch"></asp:CompareValidator>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <hr />
        <asp:Button ID="changePassword" class="submit" type="submit" runat="server" Text="Change Password" OnClick="button1_Click" />
        <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
    </div>
</asp:Content>
