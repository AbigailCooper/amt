﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Level4.aspx.cs" Inherits="AMT2.Level4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Home.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Level 4 Standard</h1>
    <br />
    <asp:Label runat="server" CssClass="label2">Click <span><a target="_self" href="https://system.learningassistant.com/bae/">here</a></span> for a link to Learning Assistant.</asp:Label>
    <br /><br /><br />
    <asp:Label runat="server" CssClass="answer" Text="Number of Logs that should be completed:        "></asp:Label>
    <asp:Label runat="server" CssClass="label" Text="" ID="targetNumLogs"></asp:Label>
    <asp:Label runat="server" Text="          "></asp:Label>
    <asp:Button runat="server" Text="Record an added Log" ID="recordLog" OnClick="recordLog_Click" />
    <br />
    <asp:Label runat="server" CssClass="answer" Text="Number of Logs you have done:         "></asp:Label>
    <asp:Label runat="server" CssClass="label" Text="" ID="logsCompleted"></asp:Label>
    <br /><br />
    <asp:Label runat="server" CssClass="answer" Text="Number of Knowlegde Questions that should be completed:        "></asp:Label>
    <asp:Label runat="server" CssClass="label" Text="" ID="targetNumKQ"></asp:Label>
    <asp:Label runat="server" Text="          "></asp:Label>
    <asp:Button runat="server" Text="Record an added Knowledge Question Unit" ID="recordKQ" OnClick="recordKQ_Click" />
    <br />
    <asp:Label runat="server" CssClass="answer" Text="Number of Knowledge Questions you have done:         "></asp:Label>
    <asp:Label runat="server" CssClass="label" Text="" ID="numOfKQ"></asp:Label>
</asp:Content>
