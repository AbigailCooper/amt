﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WorkPlacement.aspx.cs" Inherits="AMT2.WorkPlacement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Home.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />

    <div id="heading1">
        <h2>Work Placement</h2>
    </div>
    <div>
        <asp:Label runat="server" ID="addNewPlacement" Visible="false"></asp:Label>
        <asp:Button runat="server" ID="newPlaceBtn" Text="Add Placement" Visible="false" OnClick="newPlaceBtn_Click"/>
    </div>
    <br />
    <div>
        <asp:Label runat="server" CssClass="workPlacementDesc" Text="This is a timeline of your placement. At various stages throughout your placement, different tasks have to be completed. The buttons next to the time frames, will direct you to these elements."></asp:Label>
    </div>
    <br />
    <div class="mainbody" id="timeline">
        
        <asp:Button CssClass="extraThroughout" runat="server" ID="extraThroughout" Visible="false" OnClick="extraThroughout_Click" Text="Add mapping to the UKSPEC"/>
       
        <asp:Button runat="server" CssClass="extrafinal" ID="extraFinal" Visible="false" OnClick="extraFinal_Click" Text="Review your Objectives" />

        <img src="img/timeline.png" alt="timeline" />
        <asp:Label runat="server" ID="objectiveTitle" CssClass="objectiveTitle" >Objectives for your current Placement: </asp:Label>
        <asp:TextBox runat="server" class="objectives" ID="objectives" TextMode="MultiLine" Columns="25"></asp:TextBox>
        <h4 class="test"><span>First two weeks</span></h4>
        <h4 class="throughout"><span>Throughout</span></h4>
        <h4 class="week12"><span>12 Weeks</span></h4>
        <h4 class="finalMonth"><span>Final Month</span></h4>

        <asp:button runat="server" Text="Placement Induction" CssClass="placementInduction" id="PlacementInduction" OnClick="PlacementInduction_Click" />
       
        <asp:Button runat="server" Text="12 Week Review" CssClass="extra12week" ID="week12" OnClick="week12_Click" />
    </div>
    

</asp:Content>
