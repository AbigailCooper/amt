﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMT2
{
    public class DBConnection
    {
        string ConnectionLoc = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True";
        SqlConnection con;

        //Create the connection to the database.
        public void OpenConection()
        {
            con = new SqlConnection(ConnectionLoc);
            con.Open();
        }

        //Close the connection to the database.
        public void CloseConnection()
        {
            con.Close();
        }

        //Execute commands - insert, delete, update
        public int Execute(SqlCommand Query)
        {
            int i = Query.ExecuteNonQuery();
            return i;
        }

        //Gets data from the database to display on the page.
        public SqlDataReader GetData(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
    }
}