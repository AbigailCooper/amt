﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PlacementInductionManager.aspx.cs" Inherits="AMT2.PlacementInductionManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    
    <div class="placementInductionForm">
        <p>You must discuss a set of 5 objectives with your apprentice within the first 2 weeks of them starting the placement. You may add comments below. </p>
        <br />
        <asp:Label runat="server" Text="Select an Apprentice: "></asp:Label>
        <asp:DropDownList runat="server" CssClass="dropdownapp2" ID="apprentices" AutoPostBack="true" OnSelectedIndexChanged="apprentices_SelectedIndexChanged"></asp:DropDownList>
        <asp:Label runat="server" ID="error" Visible="false" Style="position:absolute; top: 204px; left: 351px;"></asp:Label>
        <br />
        <br />
        <asp:Label runat="server" CssClass="label" ID="obj1"></asp:Label><br />
        <asp:Label runat="server" CssClass="label" ID="obj2"></asp:Label><br />
        <asp:Label runat="server" CssClass="label" ID="obj3"></asp:Label><br />
        <asp:Label runat="server" CssClass="label" ID="obj4"></asp:Label><br />
        <asp:Label runat="server" CssClass="label" ID="obj5"></asp:Label><br />
    </div>
    <div class="notes">
        <br />
        <h2>Extra Notes</h2>
        <br />
        <asp:TextBox runat="server" ID="Update" class="updates" Text="Write update here..." BackColor="LightSlateGray" ></asp:TextBox>
        <br />
        <asp:Button runat="server" ID="SaveUpdate" CssClass="submitupdates" OnClick="SaveUpdate_Click" Text="Save Update" />
        <br /><br />
        <%-- Display the updates in a gridview --%>
        <asp:Panel runat="server" CssClass="panelUpdate">
            <asp:GridView runat="server" ID="objUpdates" class="objUpdates" ShowHeader="false" AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Updates Yet.">
                <Columns>
                    <asp:BoundField DataField="username" />
                    <asp:BoundField DataField="note" />
                    <asp:BoundField DataField="entryDate" />
                </Columns>

            </asp:GridView>

        </asp:Panel>
    </div>
</asp:Content>
