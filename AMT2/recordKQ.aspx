﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="recordKQ.aspx.cs" Inherits="AMT2.recordKQ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Level4.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Record Knowledge Questions</h1>
    <br />
    <asp:Label runat="server" CssClass="label">Please tick the modules that you have completed: </asp:Label>
    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
        <asp:ListItem Value="mod001">001</asp:ListItem>
        <asp:ListItem Value="mod026">026</asp:ListItem>
        <asp:ListItem Value="mod034">034</asp:ListItem>
        <asp:ListItem Value="mod068">068</asp:ListItem>
        <asp:ListItem Value="mod077">077</asp:ListItem>
        <asp:ListItem Value="mod114">114</asp:ListItem>
        <asp:ListItem Value="mod115">115</asp:ListItem>
        <asp:ListItem Value="mod116">116</asp:ListItem>
        <asp:ListItem Value="mod117">117</asp:ListItem>
        <asp:ListItem Value="mod119">119</asp:ListItem>
        <asp:ListItem Value="mod120">120</asp:ListItem>
    </asp:CheckBoxList>
    <br />
    <asp:Button runat="server" CssClass="submit" ID="save" Text="Save" OnClick="save_Click"/>

</asp:Content>
