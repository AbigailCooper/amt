﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class PIRF : System.Web.UI.Page
    {
        string usernameS;
        string apprenticeID;

        List<string> placementIDs = new List<string>();
        List<string> placementNames = new List<string>();
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //----------
            //Get userID
            //----------
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            //------------------------
            //Fill in USKPECdata table
            //------------------------
            String query = "SELECT date, location, eventName, eventLead, timeSpent, BehavioursDeveloped FROM ukspec WHERE apprenticeID='";
            query += apprenticeID + "' ORDER BY date ASC";
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                UKSPECdata.DataSource = dt;
                UKSPECdata.DataBind();
            }

            //------------------
            //Fill in objectives
            //------------------
            SqlCommand com2 = new SqlCommand("SELECT placementID FROM apprenticePlacement WHERE apprenticeID=@appID", con);
            com2.Parameters.AddWithValue("@appID", apprenticeID);
            con.Open();
            SqlDataReader reader1 = com2.ExecuteReader();

            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    string placementID = reader1.GetValue(0).ToString();
                    placementIDs.Add(placementID);
                }
                reader1.NextResult();

            }
            con.Close();

            //Get placement names using placement ID
            for (int i = 0; i < placementIDs.Count; i++)
            {
                SqlCommand com4 = new SqlCommand("SELECT placementName FROM placement WHERE placementID=@placementID", con);
                com4.Parameters.AddWithValue("@placementID", placementIDs[i]);
                con.Open();
                placementNames.Add(com4.ExecuteScalar().ToString());
                con.Close();
            }
            


            //Fill nested Gridview

            string placement1Obj = "";
            string placement2Obj = "";
            string placement3Obj = "";
            string placement4Obj = "";
            string placement5Obj = "";

            con.Open();
            for (int i = 0; i < placementIDs.Count; i++)
            {
                SqlCommand cmd6 = new SqlCommand("SELECT appPlacementID FROM apprenticePlacement WHERE apprenticeID=@appID AND placementID=@placeID", con);
                cmd6.Parameters.AddWithValue("@appID", apprenticeID);
                cmd6.Parameters.AddWithValue("@placeID", placementIDs[i].ToString());
                string appPlacementID = "";
                try
                {
                    appPlacementID = cmd6.ExecuteScalar().ToString();
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //error
                    error.Visible = true;
                    error.Text = "An error has occured.";
                }

                SqlCommand cmd5 = new SqlCommand("SELECT objectives FROM placementInduction WHERE appPlacementID=@appPlacement", con);
                cmd5.Parameters.AddWithValue("@appPlacement", appPlacementID);
                string objs = "";
                try
                {
                    objs = cmd5.ExecuteScalar().ToString();
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //error
                    error.Visible = true;
                    error.Text = "An error has occured.";
                }

                char[] separator = { '$' };
                Int32 count = 5;
                String[] strlist = null;
                if (objs != string.Empty)
                {
                    strlist = objs.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
                }

                for (int j = 0; j < strlist.Length; j++)
                {
                    if (i == 0)
                    {
                        String num = (j + 1).ToString();
                        placement1Obj += num + ". ";
                        placement1Obj += strlist[j].ToString() + "\r\n";
                    }
                    else if (i == 1)
                    {
                        String num = (j + 1).ToString();
                        placement2Obj += num + ". ";
                        placement2Obj += strlist[j].ToString() + "\r\n";
                    }
                    else if (i == 2)
                    {
                        String num = (j + 1).ToString();
                        placement3Obj += num + ". ";
                        placement3Obj += strlist[j].ToString() + "\r\n";
                    }
                    else if (i == 3)
                    {
                        String num = (j + 1).ToString();
                        placement4Obj += num + ". ";
                        placement4Obj += strlist[j].ToString() + "\r\n";
                    }
                    else if (i == 4)
                    {
                        String num = (j + 1).ToString();
                        placement5Obj += num + ". ";
                        placement5Obj += strlist[j].ToString() + "\r\n";
                    }
                }

            }
            con.Close();

            List<string> objectives = new List<string>();
            objectives.Add(placement1Obj);
            objectives.Add(placement2Obj);
            objectives.Add(placement3Obj);
            objectives.Add(placement4Obj);
            objectives.Add(placement5Obj);

            Placement1.Text = placementNames[0];
            try
            {
                Placement2.Text = placementNames[1];
            }
            catch(ArgumentOutOfRangeException)
            {
                Placement2.Text = "";
            }
            try
            {
                Placement3.Text = placementNames[2];
            }
            catch(ArgumentOutOfRangeException)
            {
                Placement3.Text = "";
            }
            try
            {
                Placement4.Text = placementNames[3];
            }
            catch(ArgumentOutOfRangeException)
            {
                Placement4.Text = "";
            }
            try
            {
                Placement5.Text = placementNames[4];
            }
            catch(ArgumentOutOfRangeException)
            {
                Placement5.Text = "";
            }

            Objective1.Text = objectives[0].ToString();
            Objective2.Text = objectives[1].ToString();
            Objective3.Text = objectives[2].ToString();
            Objective4.Text = objectives[3].ToString();
            Objective5.Text = objectives[4].ToString();


        }
        
    }
}