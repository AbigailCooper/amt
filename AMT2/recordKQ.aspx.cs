﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class recordKQ : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }
        }

        protected void save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string id = UID();
                string apprenticeID;

                //-----------------
                // Get apprenticeID
                //-----------------
                SqlCommand com = new SqlCommand("getUserID", con);
                com.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("username", usernameS);
                com.Parameters.Add(p1);
                con.Open();
                apprenticeID = com.ExecuteScalar().ToString();
                con.Close();

                bool isUserInDB = isUserData(apprenticeID);
                if (isUserInDB)
                {
                    //User is in the database so UPDATE
                    int numSelected = 0;
                    foreach (ListItem li in CheckBoxList1.Items)
                    {
                        if (li.Selected == true)
                        {
                            SqlCommand com2 = new SqlCommand("UPDATE Level4KQ SET @box = ISNULL(@box, 0) + 1 WHERE apprenticeID=@apprenticeID", con);
                            com2.Parameters.AddWithValue("@box", li.Value);
                            com2.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                            con.Open();
                            com2.ExecuteNonQuery();
                            con.Close();

                            numSelected++;
                        }
                    }

                    SqlCommand com3 = new SqlCommand("UPDATE Level4KQ SET numberOfKQ=@numselected WHERE apprenticeID=@apprenticeID", con);
                    com3.Parameters.AddWithValue("@numselected", numSelected);
                    com3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                    con.Open();
                    com3.ExecuteNonQuery();
                    con.Close();
                }
                else
                {
                    //User is not in the database to INSERT
                    List<int> list = new List<int>();
                    int numselected = 0;

                    foreach (ListItem li in CheckBoxList1.Items)
                    {
                        if (li.Selected == true)
                        {
                            list.Add(1);
                            numselected++;
                        }
                        else
                        {
                            list.Add(0);
                        }

                    }

                    SqlCommand com4 = new SqlCommand("INSERT into Level4KQ VALUES(@Id, @apprenticeID, @001, @026, @034, @068, @077, @114, @115, @116, @117, @119, @120, @numberOfKQ)", con);
                    //Add parameters.
                    com4.Parameters.AddWithValue("@Id", id);
                    com4.Parameters.AddWithValue("@apprenticeID", apprenticeID);
                    com4.Parameters.AddWithValue("@001", list[0]);
                    com4.Parameters.AddWithValue("@026", list[1]);
                    com4.Parameters.AddWithValue("@034", list[2]);
                    com4.Parameters.AddWithValue("@068", list[3]);
                    com4.Parameters.AddWithValue("@077", list[4]);
                    com4.Parameters.AddWithValue("@114", list[5]);
                    com4.Parameters.AddWithValue("@115", list[6]);
                    com4.Parameters.AddWithValue("@116", list[7]);
                    com4.Parameters.AddWithValue("@117", list[8]);
                    com4.Parameters.AddWithValue("@119", list[9]);
                    com4.Parameters.AddWithValue("@120", list[10]);
                    com4.Parameters.AddWithValue("@numberOfKQ", numselected);
                    con.Open();
                    com4.ExecuteNonQuery();
                    con.Close();
                }
                Response.Redirect("Level4.aspx");
            }
            else
            {
                //show error messages.
            }


        }

        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }

        public bool isUserData(string apprenticeID)
        {
            //---------------------------------------------
            // Check if apprentice has data in the database
            //---------------------------------------------
            SqlCommand com2 = new SqlCommand("SELECT count(Id) FROM Level4KQ WHERE apprenticeID=@apprenticeID", con);
            com2.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            int UserExist = (int)com2.ExecuteScalar();
            con.Close();

            if (UserExist > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    
    }
}