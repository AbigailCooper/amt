﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="recordLog.aspx.cs" Inherits="AMT2.recordLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Level4.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Record a Log</h1>
    <br />
    <asp:Label runat="server" CssClass="label">How many new logs have you submitted?</asp:Label>
    <asp:TextBox runat="server" CssClass="answer" ID="numOfLogs"></asp:TextBox>
    <asp:CompareValidator runat="server" ValidationGroup="g1" Operator="DataTypeCheck" Type="Integer" 
        ControlToValidate="numOfLogs" ErrorMessage="Value must be a whole number" />
    <br />
    <asp:Label runat="server" CssClass="label">Please tick the modules that the new logs cover: </asp:Label>
    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
        <asp:ListItem Value="mod001">001</asp:ListItem>
        <asp:ListItem Value="mod026">026</asp:ListItem>
        <asp:ListItem Value="mod034">034</asp:ListItem>
        <asp:ListItem Value="mod068">068</asp:ListItem>
        <asp:ListItem Value="mod077">077</asp:ListItem>
        <asp:ListItem Value="mod089">089</asp:ListItem>
        <asp:ListItem Value="mod114">114</asp:ListItem>
        <asp:ListItem Value="mod115">115</asp:ListItem>
        <asp:ListItem Value="mod116">116</asp:ListItem>
        <asp:ListItem Value="mod117">117</asp:ListItem>
        <asp:ListItem Value="mod119">119</asp:ListItem>
        <asp:ListItem Value="mod120">120</asp:ListItem>
    </asp:CheckBoxList>
    <br />
    <asp:Button runat="server" CssClass="submit" ID="save" Text="Save" OnClick="save_Click"/>

</asp:Content>
