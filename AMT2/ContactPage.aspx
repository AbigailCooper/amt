﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ContactPage.aspx.cs" Inherits="AMT2.ContactPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />

    <div id="heading1">
        <h2>Contacts</h2>
    </div>
    <div class="row">
        <div class="column">
            <asp:Panel runat="server" CssClass="card">
                <img src="img/contact-placeholder4.png" alt="person" />
                <h4>Lesley Nutter</h4>
                <p class="title">EDAS Lead</p>
                <p>Early Careers</p>
                <p class="email">lesley.nutter@bae</p>
                <p class="phone">00335123456</p>
            </asp:Panel>
        </div>

        <div class="column">
            <asp:Panel runat="server" CssClass="card">
                <img src="img/contact-placeholder7.png" alt="person" />
                <h4>Amie Tattam</h4>
                <p class="title">EDAS Lead</p>
                <p>Early Careers</p>
                <p class="email">amie.tattam@bae</p>
                <p class="phone">00335187266</p>
            </asp:Panel>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <asp:Panel runat="server" CssClass="card">
                <img src="img/contact-placeholder5.png" alt="person" />
                <h4>Graham Tasker</h4>
                <p class="title">Software Skills Coach</p>
                <p>Early Careers</p>
                <p class="email">graham.tasker@bae</p>
                <p class="phone">00335443456</p>
            </asp:Panel>
        </div>

        <div class="column">
            <asp:Panel runat="server" CssClass="card">
                <img src="img/contact-placeholder6.png" alt="person" />
                <h4>James Kenyon</h4>
                <p class="title">Aerospace Skills Coach</p>
                <p>Early Careers</p>
                <p class="email">james.kenyon@bae</p>
                <p class="phone">00335498766</p>
            </asp:Panel>
        </div>
    </div>

</asp:Content>
