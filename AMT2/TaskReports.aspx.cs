﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class TaskReports : System.Web.UI.Page
    {
        string usernameS;
        string apprenticeID;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //----------
            //Get userID
            //----------
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            //-------------------------------------------
            //Tick tickboxes if they have been completed.
            //-------------------------------------------
            SqlCommand com2 = new SqlCommand("SELECT TR1, TR2, TR3 FROM EPA WHERE apprenticeID=@appID", con);
            com2.Parameters.AddWithValue("@appID", apprenticeID);
            con.Open();
            int TR1 = 0;
            int TR2 = 0;
            int TR3 = 0;

            using (SqlDataReader reader = com2.ExecuteReader()) // fix this!
            {
                reader.Read();
                try
                {
                    TR1 = reader.GetFieldValue<int>(0);
                }
                catch (InvalidOperationException)
                {
                    TR1 = 0;    
                    TR2 = 0;
                    TR3 = 0;
                }
                try
                {
                    TR2 = reader.GetFieldValue<int>(1);
                }
                catch(InvalidOperationException)
                {
                    TR2 = 0;
                    TR3 = 0;
                }
                try
                {
                    TR3 = reader.GetFieldValue<int>(2);
                }
                catch(InvalidOperationException)
                {
                    TR3 = 0;
                }
                
            }
            con.Close();

            if (TR1 == 1) { checkbox1.Checked = true; label.Text = "You've made a good start."; }
            else { label.Text = "Let's get started!"; }
            if(TR2 == 1) { checkbox2.Checked = true; label.Text = "Nearly there!"; }
            if(TR3 == 1) { checkbox3.Checked = true; label.Text = "Congratualation, you should now have completed this section of you EPA."; }
            
        }

        protected void save_Click(object sender, EventArgs e)
        {
            int TR1 = 0;
            int TR2 = 0;
            int TR3 = 0;
            if(checkbox1.Checked) { TR1 = 1; }
            if(checkbox2.Checked) { TR2 = 1; }
            if(checkbox3.Checked) { TR3 = 1; }

            //Check if apprentice is already in database
            try
            {
                SqlCommand com = new SqlCommand("SELECT Id FROM EPA WHERE apprenticeID=@appID", con);
                com.Parameters.AddWithValue("@appID", apprenticeID);
                con.Open();
                string ID = com.ExecuteScalar().ToString();
                con.Close();

                //In database so UPDATE
                SqlCommand com2 = new SqlCommand("UPDATE EPA SET TR1=@TR1, TR2=@TR2, TR3=@TR3 WHERE apprenticeID=@appID", con);
                com2.Parameters.AddWithValue("@TR1", TR1);
                com2.Parameters.AddWithValue("@TR2", TR2);
                com2.Parameters.AddWithValue("@TR3", TR3);
                com2.Parameters.AddWithValue("@appID", apprenticeID);
                con.Open();
                int i = com2.ExecuteNonQuery();
                con.Close();

                if (i != 1)
                {
                    error.Visible = true;
                    error.Text = "Unable to save to the database.";
                }
                else if (i == 1)
                {
                    error.Visible = true;
                    error.Text = "Saved.";
                }
            }
            catch (NullReferenceException)
            {
                //Not in database so INSERT
                SqlCommand com3 = new SqlCommand("INSERT INTO EPA VALUES(@ID, @jobDesc, @short, @medium, @long, @appID, @TR1, @TR2, @TR3)", con);
                com3.Parameters.AddWithValue("@ID", UID());
                com3.Parameters.AddWithValue("@jobDesc", " ");
                com3.Parameters.AddWithValue("@short", " ");
                com3.Parameters.AddWithValue("@medium", " ");
                com3.Parameters.AddWithValue("@long", " ");
                com3.Parameters.AddWithValue("@TR1", 0);
                com3.Parameters.AddWithValue("@TR2", 0);
                com3.Parameters.AddWithValue("@TR3", 0);
                com3.Parameters.AddWithValue("@appID", apprenticeID);
                //con.Open();
                int i = com3.ExecuteNonQuery();
                con.Close();

                if (i != 1)
                {
                    error.Visible = true;
                    error.Text = "Unable to save to the database.";
                }
                else if (i == 1)
                {
                    error.Visible = true;
                    error.Text = "Saved.";
                }
            }
        }

        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }
    }
}