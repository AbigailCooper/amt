﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" <%--Inherits="AMT2.Home"--%> %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="Styles.css" rel="stylesheet" />

<div id="content">
    <asp:Label ID="firstName" runat="server" Text="First Name"></asp:Label>
    <asp:TextBox ID="nametext" runat="server"></asp:TextBox>
    <br />
    <asp:Label ID="surname" runat="server" Text="Surname"></asp:Label>
    <asp:TextBox ID="surnameText" runat="server"></asp:TextBox>
    <br />
    <asp:Label ID="scheme" runat="server" Text="Scheme"></asp:Label>
    <asp:DropDownList ID="schemeDropDown" runat="server">
        <asp:ListItem Value="Aero">Aerospace Engineering</asp:ListItem>
        <asp:ListItem Value="Software">Software Engineering</asp:ListItem>
    </asp:DropDownList>
    <br />
     <asp:Label ID="startDate" runat="server" Text="Start Date"></asp:Label>
    <asp:DropDownList ID="startDateDropDown" runat="server">
        <asp:ListItem Value="2016">2016</asp:ListItem>
        <asp:ListItem Value="2017">2017</asp:ListItem>
        <asp:ListItem Value="2018">2018</asp:ListItem>
        <asp:ListItem Value="2019">2019</asp:ListItem>
    </asp:DropDownList>

    <br />
    <br />
    <asp:Label ID="username" runat="server" Text="Username"></asp:Label>
    <asp:TextBox ID="usernameText" runat="server"></asp:TextBox>
    <br />
    <asp:Label ID="password" runat="server" Text="Password"></asp:Label>
    <asp:TextBox ID="passwordText" runat="server"></asp:TextBox> <%-- Put another password field in for validation --%> 


    <asp:Button ID="submit" runat="server" Text="Submit" OnClick="submit_Click" />

    <asp:Label ID="Label1" runat="server" Text="Password"></asp:Label>
    
</div>


</asp:Content>
