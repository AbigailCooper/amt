﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="AMT2.Home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="Styles.css" rel="stylesheet" />

<div id="content">
    <div id="heading1">
        <br />
        <h2>Key Elements of the Scheme</h2>
    </div>
    <div class="boxes">
        <div class="gallery">
            <asp:ImageButton runat="server" AlternateText="Work Placement" ID="WorkPlacementTile" ImageUrl="~/img/PlacementTile.png" OnClick="WorkPlacementTile_Click" />
        </div>
    </div>
    <div class="boxes">
        <div class="gallery">
            <asp:ImageButton runat="server" AlternateText="Level 4" ID="Level4Tile" ImageUrl="~/img/Level4Tile.png" OnClick="Level4Tile_Click" />
        </div>
    </div>
    <div class="boxes">
        <div class="gallery">
            <asp:ImageButton runat="server" AlternateText="EPA" ID="EPATile" ImageUrl="~/img/EPATile.png" OnClick="EPATile_Click" />
        </div>
    </div>
    <div class="boxes">
        <div class="gallery">
            <asp:ImageButton runat="server" AlternateText="UKSPEC" ID="UKSPECTile" ImageUrl="~/img/ukspecTile.png" OnClick="UKSPECTile_Click" />
        </div>
    </div>
    <br />
    <asp:Label runat="server" CssClass="error" ID="error" Visible="false"></asp:Label>
    
    <div class="clear"></div>

    <div id="heading2">
        <h2>News & Useful Information</h2>
    </div>
    <div class="news">
        <a href="news1.aspx" target="_self" class="updateButton">Update 8th April - Coronavirus Update</a>

    </div>
</div>


</asp:Content>
