﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="12weekReview.aspx.cs" Inherits="AMT2._12weekReview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">12 Week Review</h1>

    <asp:Label runat="server">Select an apprentice</asp:Label> <%-- Manager selects the apprentice they wish to review --%>
    <asp:DropDownList runat="server" CssClass="dropdownapp2" ID="apprentices" AutoPostBack="true" OnSelectedIndexChanged="apprentices_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:Label runat="server" ID="errorMessage" CssClass="error" Visible="false"></asp:Label>
    <br /><br /><br /><br />
    <br />
    <%-- A table, similar to that of the current physical form, to allow the manager to review the apprentices. --%>
    <asp:Table runat="server" ID="behaviourMatrix">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>

            </asp:TableHeaderCell>
            <asp:TableHeaderCell>
                Excellent
            </asp:TableHeaderCell>
            <asp:TableHeaderCell>
                Good
            </asp:TableHeaderCell>
            <asp:TableHeaderCell>
                Requires Improvement
            </asp:TableHeaderCell>
            <asp:TableHeaderCell>
                Under performing
            </asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow ID="HSrow">
            <asp:TableCell>
                Health and Safety
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="HSExcellent" runat="server" GroupName="healthAndSafety" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="HSGood" runat="server" GroupName="healthAndSafety" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="HSRequireI" runat="server" GroupName="healthAndSafety" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="HSUnder" runat="server" GroupName="healthAndSafety" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="KUrow">
            <asp:TableCell>
                Knowlegde and Understanding
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="KUExcellent" runat="server" GroupName="knowledgeUnderstanding" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="KUGood" runat="server" GroupName="knowledgeUnderstanding" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="KURequireI" runat="server" GroupName="knowledgeUnderstanding" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="KUUnder" runat="server" GroupName="knowledgeUnderstanding" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="DProw">
            <asp:TableCell>
                Design Processes
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="DPExcellent" runat="server" GroupName="designProcesses" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="DPGood" runat="server" GroupName="designProcesses" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="DPRequireI" runat="server" GroupName="designProcesses" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="DPUnder" runat="server" GroupName="designProcesses" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="RMrow">
            <asp:TableCell>
                Responsibility and Management
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="RMExcellent" runat="server" GroupName="responsibilityMan" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="RMGood" runat="server" GroupName="responsibilityMan" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="RMRequireI" runat="server" GroupName="responsibilityMan" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="RMUnder" runat="server" GroupName="responsibilityMan" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="CSrow">
            <asp:TableCell>
                Communication Skills
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="CSExcellent" runat="server" GroupName="commsSkills" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="CSGood" runat="server" GroupName="commsSkills" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="CSRequireI" runat="server" GroupName="commsSkills" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="CSUnder" runat="server" GroupName="commsSkills" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="PCrow">
            <asp:TableCell>
                Professional Commitment
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="PCExcellent" runat="server" GroupName="profCommit" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="PCGood" runat="server" GroupName="profCommit" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="PCRequireI" runat="server" GroupName="profCommit" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="PCUnder" runat="server" GroupName="profCommit" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="EDrow">
            <asp:TableCell>
                Equality and Diversity
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="EDExcellent" runat="server" GroupName="equalityDiv" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="EDGood" runat="server" GroupName="equalityDiv" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="EDRequireI" runat="server" GroupName="equalityDiv" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="EDUnder" runat="server" GroupName="equalityDiv" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="Arow">
            <asp:TableCell>
                Attendance
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="AExcellent" runat="server" GroupName="attendance" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="AGood" runat="server" GroupName="attendance" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="ARequireI" runat="server" GroupName="attendance" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButton ID="AUnder" runat="server" GroupName="attendance" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <%-- Textbox for the manager's general comments of the apprentices performance over past 12 weeks --%>
    <asp:Label runat="server" CssClass="label">General Comments: </asp:Label>
    <asp:TextBox runat="server" ID="generalComments" type="text"></asp:TextBox>
    <br />
    <br />
    <asp:Button runat="server" ID="submitReview" CssClass="submit" Text="Save" OnClick="submitReview_Click" />
    <br />
    <br />
    <asp:Label runat="server" ID="lbl_Message" Visible="false"></asp:Label>

</asp:Content>
