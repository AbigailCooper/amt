﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PlacementInduction.aspx.cs" Inherits="AMT2.PlacementInduction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="WorkPlacement.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    
    <div class="placementInductionForm">
        <p>The following must be discussed and recorded within the first two weeks of starting a new placement: </p>
        <asp:Table runat="server" Width="1000px" Height="517px" style="margin-right: 4px">
            <asp:TableRow>
                <asp:TableCell>
                    Fire exits and Assembly points
                </asp:TableCell>
                <asp:TableCell>
                    <asp:CheckBox runat="server" class="checkbox" ID="fireExits"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Fire Marshalls
                </asp:TableCell>
                <asp:TableCell>
                    <asp:CheckBox runat="server" class="checkbox" ID="fireMarshall"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    First Aiders
                </asp:TableCell>
                <asp:TableCell>
                    <asp:CheckBox runat="server" class="checkbox" ID="firstAiders"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    SHE Officers
                </asp:TableCell>
                <asp:TableCell>
                    <asp:CheckBox runat="server" class="checkbox" ID="SHE"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Meet the Team
                </asp:TableCell>
                <asp:TableCell>
                    <asp:CheckBox runat="server" class="checkbox" ID="meetTeam"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Make at least 5 placement objectives:
                </asp:TableCell>
                <asp:TableCell>
                    1. <asp:TextBox runat="server" Class="obj" ID="obj1"> </asp:TextBox><br />
                    2. <asp:TextBox runat="server" Class="obj" ID="obj2"> </asp:TextBox><br />
                    3. <asp:TextBox runat="server" Class="obj" ID="obj3"> </asp:TextBox><br />
                    4. <asp:TextBox runat="server" Class="obj" ID="obj4"> </asp:TextBox><br />
                    5. <asp:TextBox runat="server" Class="obj" ID="obj5"> </asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br />
        <asp:Button runat="server" ID="SaveObjs" Class="submit" OnClick="Save_objectives" Text="Save"/>
        <br /><br />
        <asp:Label runat="server" ID="ErrorMessage" Class="ErrorMessage" Enabled="false"></asp:Label>

        <br />
        <h2>Extra Notes</h2>
        <br />
        <asp:TextBox runat="server" ID="Update" class="updates" Placeholder="Write update here..." BackColor="LightSlateGray" ></asp:TextBox>
        <br />
        <asp:Button runat="server" ID="SaveUpdate" CssClass="submitupdates" OnClick="SaveUpdate_Click" Text="Save Update" />
        <br /><br />
        <%-- Display the updates in a gridview --%>
        <asp:Panel runat="server" CssClass="panelUpdate">
            <asp:GridView runat="server" ID="objUpdates" class="objUpdates" ShowHeader="false" AutoGenerateColumns="false" RowStyle-CssClass="rows" EmptyDataText="No Updates Yet.">
                <Columns>
                    <asp:BoundField DataField="username" />
                    <asp:BoundField DataField="note" />
                    <asp:BoundField DataField="entryDate" />
                </Columns>

            </asp:GridView>

        </asp:Panel>

    </div>
</asp:Content>
