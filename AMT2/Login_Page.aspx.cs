﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Security;

namespace AMT2
{
    public partial class Login_Page : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {

            SqlCommand com = new SqlCommand("CUser", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;
            
            string encrypPass = Encryptpass(password.Text);

            SqlParameter p1 = new SqlParameter("username", username.Text);
            SqlParameter p2 = new SqlParameter("password", encrypPass);
            com.Parameters.Add(p1);
            com.Parameters.Add(p2);
            con.Open();
            SqlDataReader rd = com.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                con.Close();
                Label1.Text = "Login successful.";
                Label1.Visible = true;
                Session["username"] = username.Text;
                Session["password"] = encrypPass;
                Response.Redirect("Home.aspx");
            }
            else
            {
                con.Close();
                Label1.Text = "Invalid username or password.";
                Label1.Visible = true;
            }
        }

        string Encryptpass(string password)
        {
            string msg = "";
            byte[] encode = new byte[password.Length];
            encode = System.Text.Encoding.UTF8.GetBytes(password);
            msg = Convert.ToBase64String(encode);
            return msg;
        }

        public bool SessionLogin()
        {
            bool isUserExists = false;
            SqlCommand cmd = new SqlCommand("ValidateUser");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@username", username.Text);
            cmd.Parameters.AddWithValue("@password", password.Text);
            cmd.Connection = con;
            con.Open();
            isUserExists = Convert.ToBoolean(cmd.ExecuteScalar());
            con.Close();
            return isUserExists;
        }
        
    }
}