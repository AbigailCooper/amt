﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class Level4 : System.Web.UI.Page
    {
        string usernameS;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            //-------------------
            //Force user to login
            //-------------------
            if (Session["username"] != null)
            {
                usernameS = Session["username"].ToString();
            }
            else
            {
                Response.Redirect("Login_Page.aspx");
            }

            //---------------------------------------------------
            //Calculate where the apprentice should be with logs.
            //---------------------------------------------------

            string apprenticeID;

            //-----------------
            // Get apprenticeID
            //-----------------
            SqlCommand com = new SqlCommand("getUserID", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter p1 = new SqlParameter("username", usernameS);
            com.Parameters.Add(p1);
            con.Open();
            apprenticeID = com.ExecuteScalar().ToString();
            con.Close();

            int targetLogs = 0; //targetNumLogs
            int targetKQ = 0; //targetNumKQ
            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year;

            SqlCommand com2 = new SqlCommand("SELECT startDate FROM apprentices WHERE uID=@uid", con);
            com2.Parameters.AddWithValue("@uid", apprenticeID);
            con.Open();
            string startDate = com2.ExecuteScalar().ToString();
            con.Close();

            int startYear = int.Parse(startDate);

            switch(currentYear - startYear)
            {
                case 1:
                    //first year
                    targetLogs = 2;
                    targetKQ = 3;
                    break;
                case 2:
                    //second year
                    targetLogs = 4;
                    targetKQ = 7;
                    break;
                case 3:
                    //third year
                    targetLogs = 6;
                    targetKQ = 11;
                    break;
                case 4:
                    //final year
                    targetLogs = 7;
                    targetKQ = 11;
                    break;                    
            }

            targetNumLogs.Text = targetLogs.ToString();
            targetNumKQ.Text = targetKQ.ToString();

            //-------------------------
            // Number of completed logs
            //-------------------------
            SqlCommand com3 = new SqlCommand("SELECT numberOfLogs FROM Level4 WHERE apprenticeID=@apprenticeID", con);
            com3.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            string logsCompletedstring = com3.ExecuteScalar().ToString();
            con.Close();
            
            logsCompleted.Text = logsCompletedstring;

            //-----------------------
            // Number of completed KQ
            //-----------------------
            SqlCommand com4 = new SqlCommand("SELECT numberOfKQ FROM Level4KQ WHERE apprenticeID=@apprenticeID", con);
            com4.Parameters.AddWithValue("@apprenticeID", apprenticeID);
            con.Open();
            string numberofKQ = com4.ExecuteScalar().ToString();
            con.Close();

            numOfKQ.Text = numberofKQ;
        }

        protected void recordLog_Click(object sender, EventArgs e)
        {
            Response.Redirect("recordLog.aspx");
        }

        protected void recordKQ_Click(object sender, EventArgs e)
        {
            Response.Redirect("recordKQ.aspx");
        }
    }
}