﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="news1.aspx.cs" Inherits="AMT2.news1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Home.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">Coronavirus Update</h1>
    <h2 id="dateHeading">April 8th</h2>
    <h2 id="subheading">Message from Sir Roger Carr, Chairman</h2>
    <br />
    <br />
    <asp:Image runat="server" ImageUrl="~/img/sirRogerCarr.jpg" AlternateText="Sir Roger Carr" />
    <br />
    <br />
    <p id="newsStory">
        As we approach the holiday weekend, I wanted to write on behalf of the Board to thank all our stakeholders who are supporting our Company in challenging times.<br />
 
    First and foremost, we have prioritised the safety and well-being of our employees above all else.  Whilst we have empowered many with the technology to work from home, there are colleagues all over the world who must attend their normal places of work, every day, to produce products and deliver services that are critical to their nations’ interests.  We cannot thank them enough for their dedication, courage and commitment.<br />
 
    Our pensioners are also remembered for all that they have done in helping to build this fine Company and our decision to inject a lump sum of capital in to the pension fund, subject to market conditions, remains a priority in spite of the pandemic turbulence.<br />
 
    Our customers world-wide and the pride we have in protecting and serving those who serve and protect us remains the driving force behind all our endeavours, and our efforts to support them all is undiminished by the difficulties that Covid- 19 has brought to us all.<br />
 
    Our supply chains are the critical links which makes all that we do possible.  Meeting our payment obligations and minimising disruption by working together in close harmony is the bedrock of our relationships and will be the cornerstone of our collective success in the months ahead.<br />
 
    Particular thanks are due to all the governments we serve across the world for their consistent cooperation and willingness to work in the spirit of partnership as we jointly navigate through these uncharted and turbulent waters – it will not be forgotten.<br />
 
    And finally, our shareholders large and small, whose loyalty, understanding and willingness to look beyond the immediate issues of the day in order to preserve a robust and sustainable business for the future is appreciated, by both the Board and management team.  We will honour our promise that dividends will be paid when conditions permit.<br />
 
    BAE Systems is a strong company with remarkable stakeholders, a substantial order book, an enviable customer base, and built on firm financial fundamentals.  The Board I am privileged to chair and the management team led by Charles Woodburn are all committed to all our stakeholders in underpinning this position today and strengthening it in the years ahead.  We thank you for all your help and support in these difficult times and hope that you and your families remain safe and well as we look forward to the better days that lie ahead.<br />
 
       <br /> <b>Sir Roger Carr</b><br />
        Chairman
    </p>
    <br />
    <br />

</asp:Content>
