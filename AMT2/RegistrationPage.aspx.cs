﻿using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace AMT2
{
    public partial class RegistrationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                // ------ Fill the placement dropdown with data from the database ------
                string select = "SELECT placementID, placementName FROM placement";

                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");
                SqlCommand cmdddl = new SqlCommand(select, con);
                SqlDataReader reader;

                try
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = "<Select Placement>"; //Default
                    newItem.Value = "0";
                    placementsDropDown.Items.Add(newItem);

                    con.Open();
                    reader = cmdddl.ExecuteReader();
                    while (reader.Read())
                    {
                        // Adding items to dropdown
                        newItem = new ListItem();
                        newItem.Text = reader["placementName"].ToString();
                        newItem.Value = reader["placementID"].ToString();
                        placementsDropDown.Items.Add(newItem);
                    }
                    reader.Close();
                    con.Close();
                }
                catch (Exception err)
                {
                    // throw execption
                }


                userType.Attributes.Add("onchange", "javascript:return userType()");
            }
            
        }

        

        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\abiga\source\repos\ApprenticeManagementTool\AMT2\App_Data\Database.mdf;Integrated Security=True");

        public void submit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string encrypPass = Encryptpass(passwordText.Text);
                bool isUsernameExist = checkUsername(usernameText.Text);
                string uniqueID = UID();
                string privilege = getPrivilege(privilegeText.Text);
                if (privilege == null)
                {
                    Label1.Text = "Invaild registration code. Please try again.";
                }
                else
                {
                    if (!isUsernameExist) // Username doesnt exist so can insert
                    {
                        int j = 0;
                        int k = 0;

                        SqlCommand cmd = new SqlCommand("insert into users values(@uID, @firstName, @surname, @username, @password, @privilege)", con);

                        cmd.Parameters.AddWithValue("@uID", uniqueID);
                        cmd.Parameters.AddWithValue("@firstName", nametext.Text);
                        cmd.Parameters.AddWithValue("@surname", surnameText.Text);
                        cmd.Parameters.AddWithValue("@username", usernameText.Text);
                        cmd.Parameters.AddWithValue("@password", encrypPass);
                        cmd.Parameters.AddWithValue("@privilege", privilege);

                        con.Open();
                        int i = cmd.ExecuteNonQuery();
                        con.Close();

                        if (userType.SelectedIndex == 0)
                        {
                            SqlCommand cmd2 = new SqlCommand("insert into apprentices values(@uID, @scheme, @startDate)", con);
                            cmd2.Parameters.AddWithValue("@uID", uniqueID);
                            cmd2.Parameters.AddWithValue("@scheme", schemeDropDown.SelectedValue);
                            cmd2.Parameters.AddWithValue("@startDate", startDateDropDown.SelectedValue);
                            con.Open();
                            j = cmd2.ExecuteNonQuery();
                            con.Close();
                        }
                        else if (userType.SelectedIndex == 1)
                        {
                            SqlCommand cmd3 = new SqlCommand("insert into managers values(@uID, @placementID)", con);
                            cmd3.Parameters.AddWithValue("@uID", uniqueID);
                            cmd3.Parameters.AddWithValue("@placementID", placementsDropDown.SelectedValue);
                            con.Open();
                            k = cmd3.ExecuteNonQuery();
                            con.Close();
                        }

                        if (i != 0 || j != 0)
                        {
                            Label1.Text = "Registration Successful";
                            //Direct to Login page
                            Session["name"] = usernameText.Text;
                            Response.Redirect("Login_Page.aspx");
                        }
                        else
                        {
                            Label1.Text = "Error while Registering";
                        }
                    }
                    else
                    {
                        //The username already exists
                        errorUsername.Visible = true;
                        errorUsername.Text = "This username already exists, please change.";
                    }
                }
            }
        }

        public bool checkUsername(string usernameText)
        {
            string username = usernameText;
            SqlCommand com = new SqlCommand("SELECT count(*) FROM users WHERE username=@username", con);
            com.Parameters.AddWithValue("@username", username);
            con.Open();
            int exists = (int)com.ExecuteScalar();
            con.Close();
            if (exists != 0)
            {
                return true; // the username is already taken
            }
            else
            {
                return false; // username does not exist
            }
        }

        public string getPrivilege(string privText)
        {
            string privilege = privText;
            if (privilege == "apprenticeLog01")
            {
                return "apprentice";
            }
            else if (privilege == "managerApproved_01")
            {
                return "manager";
            }
            else if(privilege == "EarlyCareers")
            {
                return "earlyCareers";
            }
            else
            {
                return null;
            }
        }

        string UID()
        {
            string uniqueID = System.Guid.NewGuid().ToString();
            return uniqueID;
        }
        
        string Encryptpass(string password)
        {
            string msg = "";
            byte[] encode = new byte[password.Length];
            encode = System.Text.Encoding.UTF8.GetBytes(password);
            msg = Convert.ToBase64String(encode);
            return msg;
        }

        protected void userType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (userType.SelectedIndex == 1) // apprentice
            {
                apprenticesOnly.Visible = true;
                scheme.Visible = true;
                schemeDropDown.Visible = true;
                startDate.Visible = true;
                startDateDropDown.Visible = true;

                managersOnlylbl.Visible = false;
                placementLbl.Visible = false;
                placementsDropDown.Visible = false;
            }
            else if (userType.SelectedIndex == 2) // manager
            {
                managersOnlylbl.Visible = true;
                placementLbl.Visible = true;
                placementsDropDown.Visible = true;

                apprenticesOnly.Visible = false;
                scheme.Visible = false;
                schemeDropDown.Visible = false;
                startDate.Visible = false;
                startDateDropDown.Visible = false;

            }
            else if (userType.SelectedIndex == 3 || userType.SelectedIndex == 0) // EC
            {
                //All not visible.
                apprenticesOnly.Visible = false;
                scheme.Visible = false;
                schemeDropDown.Visible = false;
                startDate.Visible = false;
                startDateDropDown.Visible = false;

                managersOnlylbl.Visible = false;
                placementLbl.Visible = false;
                placementsDropDown.Visible = false;
            }
        }
    }
}