﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EPA.aspx.cs" Inherits="AMT2.EOP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Styles.css" rel="stylesheet" />
    <div class="backArrow">
        <a href="Home.aspx" target="_self"><img src="img/back.png" alt="Back" style="height: 50px; width: 50px; margin-top: 15px"/></a><br />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <h1 id="heading1">End Point Assessment</h1>
    <br />

    <img src="img/timelineEPA.PNG" alt="Timeline for EPA" class="timelineEPA"/>
    <br />
    <br />
    <h2>Portfolio of Evidence</h2>
    <p>It is a collection of evidence to show you understand the organisation in terms of their products, processes, procedures, tools, equipment, materials, documentation and information systems by showcasing: What you have done, 
        What you have learnt, HOW and WHY you have applied the knowledge and skills to real work tasks including solving problems encountered whilst undertaking activities in the workplace.
    </p>
    <p>The portfolio is made up of 5 different elements: a CV, a job description, task reports, a continuous personal development plan, and a Professional Indicators Recording Form (PIRF)
    </p>




    <asp:Button runat="server" CssClass="EPAButton" id="cvCreator" OnClick="cvCreator_Click" Text="CV Creator" />
    <asp:Button runat="server" CssClass="EPAButton" id="jobDescription" OnClick="jobDescription_Click" Text="Job Description" />
    <asp:Button runat="server" CssClass="EPAButton" ID="CPD" OnClick="CPD_Click" Text="CPD" />
    <asp:Button runat="server" CssClass="EPAButton" ID="taskReports" OnClick="taskReports_Click" Text="Task Reports" />
    <asp:Button runat="server" CssClass="EPAButton" ID="PIRF" OnClick="PIRF_Click" Text="PIRF" />
    <br />
    <br />


</asp:Content>
